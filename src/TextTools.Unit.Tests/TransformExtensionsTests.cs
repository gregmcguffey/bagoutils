﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools.Unit.Tests
{
    [TestFixture]
    public class TransformExtensionsTests
    {

        //-------------------------------------------------------------------------
        //
        // OrDefault Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void OrDefault_WithNull_ReturnsDefault()
        {
            // Arrange
            string nullString = null;
            var defaultValue = "<default>";

            // Act
            var returnedValue = nullString.OrDefault(defaultValue);

            // Assert
            Assert.AreEqual(expected: defaultValue, actual: returnedValue);
        }

        [Test]
        public void OrDefault_WithEmtpy_ReturnsDefault()
        {
            // Arrange
            var emptyString = string.Empty;
            var defaultValue = "<default>";

            // Act
            var returnedValue = emptyString.OrDefault(defaultValue);

            // Assert
            Assert.AreEqual(expected: defaultValue, actual: returnedValue);
        }

        [Test]
        public void OrDefault_WithText_ReturnsText()
        {
            // Arrange
            // Arrange
            var originalValue = "any";
            var defaultValue = "<default>";

            // Act
            var returnedValue = originalValue.OrDefault(defaultValue);

            // Assert
            Assert.AreEqual(expected: originalValue, actual: returnedValue);
        }


        //-------------------------------------------------------------------------
        //
        // CollapseWhitespace Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void CollapseWhitespace_WithManySpaces_CollapsesToSingleSpaces()
        {
            // Arrange
            var originalText = "this     has    lots of   spaces.";
            var expected = "this has lots of spaces.";

            // Act
            var actual = originalText.CollapseWhitespace();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void CollapseWhitespace_WithLeadingSpaces_TrimsBeginning()
        {
            // Arrange
            var originalText = "   this     has    lots of   spaces.";
            var expected = "this has lots of spaces.";

            // Act
            var actual = originalText.CollapseWhitespace();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void CollapseWhitespace_WithTrailingSpaces_TrimsEnd()
        {
            // Arrange
            var originalText = "this     has    lots of   spaces.   ";
            var expected = "this has lots of spaces.";

            // Act
            var actual = originalText.CollapseWhitespace();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(null)]
        [TestCase("")]
        public void CollapseWhitespace_WithMissingString_ReturnsEmptyString(string missingString)
        {
            // Arrange
            var expected = string.Empty;

            // Act
            var actual = missingString.CollapseWhitespace();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        //-------------------------------------------------------------------------
        //
        // NullToEmtpyString Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void NullToEmptyString_WithNull_ReturnsEmptyString()
        {
            // Arrange
            string nullString = null;
            var expected = string.Empty;

            // Act
            var actual = nullString.NullToEmpty();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void NullToEmptyString_WithEmpty_ReturnsEmptyString()
        {
            // Arrange
            var emptyString = string.Empty;
            var expected = string.Empty;

            // Act
            var actual = emptyString.NullToEmpty();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void NullToEmptyString_WithText_ReturnsText()
        {
            // Arrange
            var text = "any";
            var expected = "any";

            // Act
            var actual = text.NullToEmpty();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        //-------------------------------------------------------------------------
        //
        // EnsurePrefixed Tests
        //
        //-------------------------------------------------------------------------

        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void EnsurePrefixed_WithNoValue_ReturnsJustDot(string value)
        {
            // Arrange
            var expected = ".";

            // Act
            var actual = value.EnsurePrefixed(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void EnsurePrefixed_WithNoDot_CorrectlyAddsDot()
        {
            // Arrange
            var value = "any";
            var expected = ".any";

            // Act
            var actual = value.EnsurePrefixed(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void EnsurePrefixed_WithDot_CorrectlyReturnsOriginal()
        {
            // Arrange
            var value = ".any";
            var expected = ".any";

            // Act
            var actual = value.EnsurePrefixed(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }


        //-------------------------------------------------------------------------
        //
        // EnsureSuffixed Tests
        //
        //-------------------------------------------------------------------------

        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void EnsureSuffixed_WithNoValue_ReturnsJustDot(string value)
        {
            // Arrange
            var expected = ".";

            // Act
            var actual = value.EnsureSuffixed(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void EnsureSuffixed_WithNoDot_CorrectlyAddsDot()
        {
            // Arrange
            var value = "any";
            var expected = "any.";

            // Act
            var actual = value.EnsureSuffixed(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void EnsureSuffixed_WithDot_CorrectlyReturnsOriginal()
        {
            // Arrange
            var value = "any.";
            var expected = "any.";

            // Act
            var actual = value.EnsureSuffixed(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }


        //-------------------------------------------------------------------------
        //
        // RemoveFromEnd Tests
        //
        //-------------------------------------------------------------------------

        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void RemoveFromEnd_WithNoValue_ReturnsEmptyString(string value)
        {
            // Arrange
            var expected = value.NullToEmpty();

            // Act
            var actual = value.RemoveFromEnd(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void RemoveFromEnd_WithNoExtraneous_ReturnOriginal()
        {
            // Arrange
            var value = "any";
            var extra = ".";
            var expected = "any";

            // Act
            var actual = value.RemoveFromEnd(extra);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void RemoveFromEnd_WithExtraneous_ReturnOriginal()
        {
            // Arrange
            var value = "any.";
            var extra = ".";
            var expected = "any";

            // Act
            var actual = value.RemoveFromEnd(extra);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test(Description = "Test if a string containing the text to remove in more than just the end, only removes the end text.")]
        public void RemoveFromEnd_WithRepeatedText_OnlyRemovesFromEnd()
        {
            // Arrange
            var value = "_extra any_extra";
            var extra = "_extra";
            var expected = "_extra any";

            // Act
            var actual = value.RemoveFromEnd(extra);

            // Assert
            Assert.AreEqual(expected, actual);
        }


        //-------------------------------------------------------------------------
        //
        // RemoveFromStart Tests
        //
        //-------------------------------------------------------------------------

        [TestCase(null)]
        [TestCase("")]
        public void RemoveFromStart_WithNoValue_ReturnsEmptyString(string value)
        {
            // Arrange
            var expected = string.Empty;

            // Act
            var actual = value.RemoveFromStart(".");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void RemoveFromStart_WithNoExtraneous_ReturnOriginal()
        {
            // Arrange
            var value = "any";
            var extra = ".";
            var expected = "any";

            // Act
            var actual = value.RemoveFromStart(extra);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void RemoveFromStart_WithoutExtraneous_ReturnOriginal()
        {
            // Arrange
            var value = ".any";
            var extra = ".";
            var expected = "any";

            // Act
            var actual = value.RemoveFromStart(extra);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test(Description = "Test if a string containing the text to remove in more than just the end, only removes the end text.")]
        public void RemoveFromStart_WithRepeatedText_OnlyRemovesFromEnd()
        {
            // Arrange
            var value = "_extra_any_extra";
            var extra = "_extra";
            var expected = "_any_extra";

            // Act
            var actual = value.RemoveFromStart(extra);

            // Assert
            Assert.AreEqual(expected, actual);
        }


        //-------------------------------------------------------------------------
        //
        // Quote Tests
        //
        //-------------------------------------------------------------------------

        [TestCase(null)]
        [TestCase("")]
        public void Quote_WithNoText_ReturnsEmptyQuotes(string value)
        {
            // Arrange
            var expected = "\"\"";

            // Act
            var actual = value.Quote();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void Quote_WithText_QuotesText()
        {
            // Arrange
            var value = "any";
            var expected = $"\"any\"";

            // Act
            var actual = value.Quote();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        //-------------------------------------------------------------------------
        //
        // SplitIntoLines Tests
        //
        //-------------------------------------------------------------------------


        [TestCase(null)]
        [TestCase("")]
        public void SplitIntoLines_WithNoText_ReturnsEmptyList(string value)
        {
            // Arrange

            // Act
            var actual = value.SplitIntoLines();

            // Assert
            Assert.That(actual, Is.Empty);
        }


        [Test]
        public void SplitIntoLines_WithSingleLine_ReturnOneItemList()
        {
            // Arrange
            var value = "a single line";
            var expectedCount = 1;

            // Act
            var asLines = value.SplitIntoLines();

            // Assert
            Assert.That(asLines, Has.Exactly(expectedCount).Items);
        }
    }
}
