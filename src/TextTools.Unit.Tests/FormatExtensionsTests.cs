﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools.Unit.Tests
{
    [TestFixture]
    public class FormatExtensionsTests
    {
        //-------------------------------------------------------------------------
        //
        // Indent Tests
        //   Text version
        //
        //-------------------------------------------------------------------------

        [Test]
        public void Indent_WithTextAndIndent_IndentsCorrectly()
        {
            // Arrange
            var defaultIndent = "    ";
            var originalText = "any";
            var expected = $"{defaultIndent}any";

            // Act
            var actual = originalText.Indent();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, " ")]
        [TestCase(3, "   ")]
        [TestCase(10, "          ")]
        public void Indent_WithTextAndIndent_IndentsCorrectly(int indentSize, string expectedIndent)
        {
            // Arrange
            var originalText = "any";
            var expected = $"{expectedIndent}any";

            // Act
            var actual = originalText.Indent(indentSize);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Indent_WithMissingText_ReturnsEmptyText(string missingText)
        {
            // Arrange
            var ignoredIndentSize = 1;
            var expected = string.Empty;

            // Act
            var actual = missingText.Indent(ignoredIndentSize);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Indent_WithZeroSize_ReturnsOriginalText()
        {
            var originalText = "any";
            var zeroIndent = 0;

            // Act
            var actual = originalText.Indent(zeroIndent);

            // Assert
            Assert.AreEqual(originalText, actual);
        }

        [Test]
        public void Indent_WithNegativeIndent_ThrowsException()
        {
            var negativeIndentMessage = "The minimum indent is zero but -1 was provided.";
            var originalText = "any";
            var badIndent = -1;

            // Act
            var ex = Assert.Throws<ArgumentOutOfRangeException>(() => originalText.Indent(badIndent));

            // Assert
            StringAssert.Contains(expected: negativeIndentMessage, actual: ex.Message);
        }


        //-------------------------------------------------------------------------
        //
        // Indent Tests
        //   Lines version
        //
        //-------------------------------------------------------------------------

        [Test]
        public void Indent_WithTextAndNoIndent_IndentsUsingDefault()
        {
            // Arrange
            var defaultIndent = "    ";
            var singleLine = new List<string> { "some line" };
            var expected = new List<string>
            {
                $"{defaultIndent}some line",
            };

            // Act
            var actual = singleLine.Indent();

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Indent_WithLineAndIndent_IndentsLine()
        {
            // Arrange
            var singleLine = new List<string> { "some line" };
            var indentSize = 3;
            var expected = new List<string>
            {
                "   some line",
            };

            // Act
            var actual = singleLine.Indent(indentSize);

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Indent_WithLinesAndIndent_IndentsAllLines()
        {
            var multiLines = new List<string>
            {
                "some line",
                "another line",
                "more line",
            };
            var indentSize = 3;
            var expected = new List<string>
            {
                "   some line",
                "   another line",
                "   more line",
            };

            // Act
            var actual = multiLines.Indent(indentSize);

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }


        [Test]
        public void Indent_WithNoLines_ReturnsEmptyList()
        {
            var emptyLines = new List<string>();
            var indentSize = 3;
            var expected = new List<string>();

            // Act
            var actual = emptyLines.Indent(indentSize);

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }


        [Test]
        public void Indent_WithNullLines_ThrowsException()
        {
            // Arrange
            var missingLinesMessage = "The lines variable is null.";
            var anyIndent = 1;
            List<string> nullLines = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullLines.Indent(anyIndent));

            // Assert
            StringAssert.Contains(expected: missingLinesMessage, actual: ex.Message);
        }
    }
}
