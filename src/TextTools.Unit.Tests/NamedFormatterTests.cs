﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools.Unit.Tests
{
    [TestFixture]
    public class NamedFormatterTests
    {
        [Test]
        public void NamedFormat_WithNull_ThrowsException()
        {
            string nullFormat = null;
            var expectedMessage = "The format string cannot be null.";

            var ex = Assert.Throws<ArgumentNullException>(() => nullFormat.NamedFormat(null));

            StringAssert.Contains(expectedMessage, ex.Message);
        }

        [Test]
        public void NamedFormat_WithFormat_ResturnCorrectResult()
        {
            var format = "Hello {value}!";
            var data = new { value = "world" };
            var expectedResult = "Hello world!";

            var actualResult = format.NamedFormat(data);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void NamedFormat_WithFormattedFormat_ResturnCorrectResult()
        {
            var format = "{Amount:C}";
            var data = new { Amount = 100 };
            var expectedResult = "$100.00";

            var actualResult = format.NamedFormat(data);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void NamedFormat_WithEscapedBraces_ReturnsCorrectResult()
        {
            var format = "{Amount:C} {{Hey}}";
            var data = new { Amount = 100 };
            var expectedResult = "$100.00 {Hey}";

            var actualResult = format.NamedFormat(data);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void NameFormat_WithNoData_ReturnsWithArgumentRemoved()
        {
            var format = "Hey {value}";
            var expectedResult = "Hey ";

            var actualResult = format.NamedFormat(null);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase("{", "No name was specified for a formatting argument and the closing bracket is missing.")]
        [TestCase("}", "Closing bracket with no matching opening bracket.")]
        [TestCase("{}", "No name was specified for a formatting argument.")]
        [TestCase("{{}", "Closing bracket with no matching opening bracket.")]
        [TestCase("{}}", "No name was specified for a formatting argument.")]
        [TestCase("{hey", "Closing bracket is missing.")]
        [TestCase("{{hey}", "Closing bracket with no matching opening bracket.")]
        [TestCase("{hey}}", "Closing bracket with no matching opening bracket.")]
        public void NamedFormat_WithBadFormatExtraBrace_ThrowsException(string format, string expectedMessage)
        {
            var ex = Assert.Throws<FormatException>(() => format.NamedFormat(null));

            StringAssert.Contains(expectedMessage, ex.Message);
        }
    }
}
