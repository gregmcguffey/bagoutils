﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools.Unit.Tests
{
    [TestFixture]
    public class PathRelatedExtensionsTests
    {
        //-------------------------------------------------------------------------
        //
        // AddPath Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AddPath_WithPaths_ReturnsCorrectCombinedPath()
        {
            // Arrange
            var expectedPath = @"c:\test\any.txt";
            var path = @"c:\test";
            var additional = "any.txt";

            // Act
            var actualPath = path.AddPath(additional);

            // Assert
            Assert.AreEqual(expectedPath, actualPath);
        }


        [Test]
        public void AddMethod_WithNullBasePath_ThrowsException()
        {
            // Arrange
            var noBasePathMessage = "The base path cannot be null.";
            string path = null;
            var additional = "any.txt";

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => path.AddPath(additional));

            // Assert
            StringAssert.Contains(expected: noBasePathMessage, actual: ex.Message);
        }


        [Test]
        public void AddMethod_WithNullAdditionalPath_ThrowsException()
        {
            // Arrange
            var noAdditionalPathMessage = "The additional path cannot be null.";
            var path = @"c:\test";
            string additional = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => path.AddPath(additional));

            // Assert
            StringAssert.Contains(expected: noAdditionalPathMessage, actual: ex.Message);
        }

        //-------------------------------------------------------------------------
        //
        // PathToAncestor Tests
        //
        //-------------------------------------------------------------------------


        [Test]
        public void Demo_Example_1()
        {
            // Arrange
            var expectedPath = @"C:\Test";
            var fullPath = @"C:\Test\Project\bin\debug";

            // Act
            var actualPath = fullPath.PathToAncestor("Project");

            // Assert
            Assert.AreEqual(expectedPath, actualPath);
        }

        [Test]
        public void Demo_Example_2()
        {
            // Arrange
            var expectedPath = @"C:\Test\Project";
            var fullPath = @"C:\Test\Project\bin\debug";

            // Act
            var actualPath = fullPath.PathToAncestor("Project", includeReferenceFolder: true);

            // Assert
            Assert.AreEqual(expectedPath, actualPath);
        }

        [TestCase("one", false, @"C:")]
        [TestCase("one", true, @"C:\one")]
        [TestCase("three", false, @"C:\one\two")]
        [TestCase("three", true, @"C:\one\two\three")]
        [TestCase("five", false, @"C:\one\two\three\four")]
        [TestCase("five", true, @"C:\one\two\three\four\five")]
        public void PathToAncestor_WithLongPathAndValidRefFolder_ReturnsCorrectPath(string refPath, bool includeRefFolder, string expectedPath)
        {
            // Arrange
            var fullPath = @"C:\one\two\three\four\five\any.txt";

            // Act
            var actualPath = fullPath.PathToAncestor(refPath, includeReferenceFolder: includeRefFolder);

            // Assert
            Assert.AreEqual(expectedPath, actualPath);
        }


        [Test]
        public void PathToAncestor_WithNoFileInPath_ReturnsCorrectPath()
        {
            // Arrange
            var expectedPath = @"C:\one\two\three\four\five";
            var fullPath = @"C:\one\two\three\four\five";

            // Act
            var actualPath = fullPath.PathToAncestor("five", includeReferenceFolder: true, noFileInPath: true);

            // Assert
            Assert.AreEqual(expectedPath, actualPath);
        }


        [Test]
        public void PathToAncetor_WithNullPath_ThrowsException()
        {
            // Arrange
            var noPathMessage = "The base path that calls this method cannot be null.";
            string fullPath = null;
            var referenceFolder = "two";

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => fullPath.PathToAncestor(referenceFolder));

            // Assert
            StringAssert.Contains(expected: noPathMessage, actual: ex.Message);
        }


        [Test]
        public void PathToAncestor_WithNoRefFolder_ThrowsException()
        {
            // Arrange
            var noReferenceFolderMessage = "The reference folder cannot be null.";
            var fullPath = @"C:\one\two\three\four\five";
            string referenceFolder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => fullPath.PathToAncestor(referenceFolder));

            // Assert
            StringAssert.Contains(expected: noReferenceFolderMessage, actual: ex.Message);
        }

        //-------------------------------------------------------------------------
        //
        // FolderList Tests
        //
        //-------------------------------------------------------------------------


        [Test]
        public void FolderList_WithDriveAndFile_ReturnsCorrectList()
        {
            // Arrange
            var expectedList = new List<string>
            {
                "one",
                "two",
                "three",
                "four",
            };
            var fullPath = @"C:\one\two\three\four\any.txt";

            // Act
            var actualList = fullPath.FolderList();

            // Assert
            CollectionAssert.AreEquivalent(expectedList, actualList);
        }

        [Test]
        public void FolderList_WhenRequestingDrive_ReturnsCorrectList()
        {
            // Arrange
            var expectedList = new List<string>
            {
                "C:",
                "one",
                "two",
                "three",
                "four",
            };
            var fullPath = @"C:\one\two\three\four\any.txt";

            // Act
            var actualList = fullPath.FolderList(FolderListParseStrategy.IgnoreFile);

            // Assert
            CollectionAssert.AreEquivalent(expectedList, actualList);
        }


        [Test]
        public void FolderList_WithNoPath_ThrowsException()
        {
            // Arrange
            var missingPathMessage = "No path was provided.";
            string fullPath = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => fullPath.FolderList());

            // Assert
            StringAssert.Contains(expected: missingPathMessage, actual: ex.Message);
        }

        //-------------------------------------------------------------------------
        //
        // ToCodeBas Tests
        //
        //-------------------------------------------------------------------------


        [TestCase(@"C:\test\any.dll", @"C:/test/any.dll")]
        [TestCase(@"z:\test\sub\any.dll", @"z:/test/sub/any.dll")]
        [TestCase(@"C:\test", @"C:/test")]
        public void ToCodeBase_WithPath_ReturnCorrectCodeBase(string file, string transformedFile)
        {
            // Arrange
            var expectedCodeBase = "File:///{0}".Fill(transformedFile);

            // Act
            var actualCodeBase = file.ToCodeBase();

            // Assert
            Assert.AreEqual(expectedCodeBase, actualCodeBase);
        }


        [Test]
        public void ToCodeBase_WithNoPath_ThrowsException()
        {
            // Arrange
            var noPathMessage = "No path was supplied.";
            string badPath = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => badPath.ToCodeBase());

            // Assert
            StringAssert.Contains(noPathMessage, ex.Message);
        }

    }
}
