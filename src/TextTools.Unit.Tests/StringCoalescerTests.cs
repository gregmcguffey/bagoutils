﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools.Unit.Tests
{
    [TestFixture]
    public class StringCoalescerTests
    {

        [TestCase("1", null, null, "1")]
        [TestCase(null, "2", null, "2")]
        [TestCase(null, null, "3", "3")]
        [TestCase(null, null, null, null)]
        [TestCase("", "4", "99", "4")]
        [TestCase("", "", "\t", "\t")]
        [TestCase("\t", "5", "", "5")]
        public void Or_ChainedStrings_ReturnsFirstNonNullOrEmpty(string item1, string item2, string item3, string expectedReturn)
        {
            var result = item1.Or(item2).Or(item3);

            Assert.AreEqual(expectedReturn, result);
        }

        [Test]
        public void Or_WithRealFirstValue_ReturnsFirstValue()
        {
            // Arrange
            string startValue = "first";
            string secondValue = "second";
            string thirdValue = "third";
            string fourthValue = "fourth";

            // Act
            var actualValue = startValue
                .Or(secondValue)
                .Or(thirdValue)
                .Or(fourthValue);

            // Assert
            Assert.AreEqual(startValue, actualValue);
        }

        [Test]
        public void Or_WithManyValues_ReturnsFirstNonNullNonEmptyNonWhitespaceOnlyValue()
        {
            // Arrange
            string startValue = null;
            string emptyValue = string.Empty;
            string whiteValue = "    ";
            string realValue = "any";

            // Act
            var actualValue = startValue
                .Or(emptyValue)
                .Or(whiteValue)
                .Or(realValue);

            // Assert
            Assert.AreEqual(realValue, actualValue);
        }

        [Test]
        public void Or_WithRealValueInMiddle_ReturnsRealValue()
        {
            // Arrange
            string startValue = null;
            string emptyValue = string.Empty;
            string whiteValue = "    ";
            string realValue = "any";

            // Act
            var actualValue = startValue
                .Or(emptyValue)
                .Or(realValue)
                .Or(whiteValue);

            // Assert
            Assert.AreEqual(realValue, actualValue);
        }

        [Test]
        public void Or_WithNoRealValues_ReturnsLastBadValue()
        {
            // Arrange
            string startValue = null;
            string emptyValue = string.Empty;
            string whiteValue = "    ";

            // Act
            var actualValue = startValue
                .Or(emptyValue)
                .Or(whiteValue);

            // Assert
            Assert.AreEqual(whiteValue, actualValue);
        }
    }
}
