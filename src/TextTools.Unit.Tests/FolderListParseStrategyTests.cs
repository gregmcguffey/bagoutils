﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools.Unit.Tests
{
    [TestFixture]
    public class FolderListParseStrategyTests
    {
        private List<string> testFullPath = new List<string>
        {
            "C:"
            ,"one"
            ,"two"
            ,"three"
            ,"four"
            ,"any.txt",
        };

        [Test]
        public void Parse_WithUseAll_ReturnsCorrectList()
        {
            // Arrange
            var expectedList = new List<string>
            {
            "C:"
            ,"one"
            ,"two"
            ,"three"
            ,"four"
            ,"any.txt",
            };
            var strategy = FolderListParseStrategy.UseAll;

            // Act
            var actualList = strategy.Parse(this.testFullPath);

            // Assert
            CollectionAssert.AreEquivalent(expectedList, actualList);
        }

    }
}
