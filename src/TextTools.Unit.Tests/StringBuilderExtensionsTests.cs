﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools.Unit.Tests
{
    [TestFixture]
    public class StringBuilderExtensionsTests
    {

        //-------------------------------------------------------------------------
        //
        // Space Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void Space_WithEmptyBuilder_AddsSpaceToText()
        {
            // Arrange
            var expected = " ";

            var builder = new StringBuilder();

            // Act
            builder.Space();
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void Space_WithLoadedBuilder_AddsSpaceToExistingText()
        {
            var baseText = "any";
            var expected = "any ";

            var builder = new StringBuilder();
            builder.Append(baseText);

            // Act
            builder.Space();
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void Space_WithMultipleSpaced_AddsCorrectSpaces()
        {
            var baseText = "any";
            int spaceCount = 5;
            var expected = "any     ";

            var builder = new StringBuilder();
            builder.Append(baseText);

            // Act
            builder.Space(spaceCount);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Space_WithZeroSpaceCount_ReturnsExitingText()
        {
            var baseText = "any";
            int spaceCount = 0;
            var expected = "any";

            var builder = new StringBuilder();
            builder.Append(baseText);

            // Act
            builder.Space(spaceCount);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(-1)]
        [TestCase(-24)]
        public void Space_WithNegativeSpaceCount_ThrowsException(int badSize)
        {
            // Arrange
            var missingBuilderMessage = $"The minimum number of spaces is zero. {badSize} is less than zero.";
            var builder = new StringBuilder();

            // Act
            var ex = Assert.Throws<ArgumentOutOfRangeException>(() => builder.Space(badSize));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }

        [Test]
        public void Space_WithNoBuilder_ThrowsException()
        {
            // Arrange
            var missingBuilderMessage = "The string builder is null.";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.Space());

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }


        //-------------------------------------------------------------------------
        //
        // BlankLine Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void BlankLine_WithExistingText_AddEmptyLine()
        {
            // Arrange
            var existingLine = "existing line";
            var expected = $"{existingLine}\r\n\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.BlankLine();
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BlankLine_WithNoBuilder_ThrowsException()
        {
            // Arrange
            var missingBuilderMessage = "The string builder is null.";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.BlankLine());

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }


        //-------------------------------------------------------------------------
        //
        // AppendDivider Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AppendDivider_WhenDefined_AddsCorrectDivider()
        {
            // Arrange
            var existingLine = "existing";
            var dividerSize = 10;
            var expected = "existing\r\n----------\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.AppendDivider(dividerSize);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void AppendDivider_WithZeroSize_DoesNotAppendAnyText()
        {
            // Arrange
            var existingLine = "existing";
            var dividerSize = 0;
            var expected = "existing\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.AppendDivider(dividerSize);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AppendDivider_WithCustomDividerChar_AddsCorrectDivider()
        {
            var existingLine = "existing";
            var dividerSize = 10;
            var customDivider = '$';
            var expected = "existing\r\n$$$$$$$$$$\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.AppendDivider(dividerSize, customDivider);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AppendDivider_WithNegativeSize_ThrowsException()
        {
            // Arrange
            var badDividerSizeMessage = $"The minimum size of a divider is zero. -1 is less than zero.";
            var existingLine = "existing";
            var badSize = -1;

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            var ex = Assert.Throws<ArgumentOutOfRangeException>(() => builder.AppendDivider(badSize));

            // Assert
            StringAssert.Contains(expected: badDividerSizeMessage, actual: ex.Message);
        }

        [Test]
        public void AppendDivider_WithNoBuilder_ThrowsException()
        {
            var missingBuilderMessage = "The string builder is null.";
            var dividerSize = 10;

            StringBuilder builder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendDivider(dividerSize));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }


        //-------------------------------------------------------------------------
        //
        // Indent Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void Indent_WithNoBuilder_ThrowException()
        {
            // Arrange
            var noBuilderMessage = "The StringBuilder is null.";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.Indent(2));

            // Assert
            StringAssert.Contains(expected: noBuilderMessage, actual: ex.Message);
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void Indent_WithSize_IndentsCorrectly(int size)
        {
            // Arrange
            var testText = "any";
            var expectedIndent = new string(' ', size);
            var expectedText = expectedIndent + testText;

            var builder = new StringBuilder();

            // Act
            builder.Indent(size).Append(testText);
            var actualText = builder.ToString();

            // Assert
            Assert.AreEqual(expectedText, actualText);
        }


        //-------------------------------------------------------------------------
        //
        // IndentIf Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void IndentIf_WithNoBuilder_ThrowException()
        {
            // Arrange
            var noBuilderMessage = "The StringBuilder is null.";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.IndentIf(true, 2));

            // Assert
            StringAssert.Contains(expected: noBuilderMessage, actual: ex.Message);
        }

        [Test]
        public void IndentIf_WithTrue_AddsIndent()
        {
            // Arrange
            var testText = "any";
            var expectedIndent = new string(' ', 2);
            var expectedText = expectedIndent + testText;
            var condition = true;

            var builder = new StringBuilder();

            // Act
            builder.IndentIf(condition, 2).Append(testText);
            var actualText = builder.ToString();

            // Assert
            Assert.AreEqual(expectedText, actualText);
        }

        [Test]
        public void IndentIf_WithFalse_DoesNotAddIndent()
        {
            // Arrange
            var testText = "any";
            var expectedIndent = new string(' ', 2);
            var expectedText = testText;
            var condition = false;

            var builder = new StringBuilder();

            // Act
            builder.IndentIf(condition, 2).Append(testText);
            var actualText = builder.ToString();

            // Assert
            Assert.AreEqual(expectedText, actualText);
        }


        //-------------------------------------------------------------------------
        //
        // IndentIfNot Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void IndentIfNot_WithNoBuilder_ThrowException()
        {
            // Arrange
            var noBuilderMessage = "The StringBuilder is null.";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.IndentIfNot(true, 2));

            // Assert
            StringAssert.Contains(expected: noBuilderMessage, actual: ex.Message);
        }

        [Test]
        public void IndentIfNot_WithFalse_AddsIndent()
        {
            // Arrange
            var testText = "any";
            var expectedIndent = new string(' ', 2);
            var expectedText = expectedIndent + testText;
            var condition = false;

            var builder = new StringBuilder();

            // Act
            builder.IndentIfNot(condition, 2).Append(testText);
            var actualText = builder.ToString();

            // Assert
            Assert.AreEqual(expectedText, actualText);
        }

        [Test]
        public void IndentIfNot_WithTrue_DoesNotAddIndent()
        {
            // Arrange
            var testText = "any";
            var expectedIndent = new string(' ', 2);
            var expectedText = testText;
            var condition = true;

            var builder = new StringBuilder();

            // Act
            builder.IndentIfNot(condition, 2).Append(testText);
            var actualText = builder.ToString();

            // Assert
            Assert.AreEqual(expectedText, actualText);
        }


        //-------------------------------------------------------------------------
        //
        // Fill Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void Fill_WithNoBuilder_ThrowsException()
        {
            var missingBuilderMessage = "The StringBuilder is null.";
            var template = "Hello {0}";
            var name = "Bob";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.Fill(template, name));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }

        [Test]
        public void Fill_WithTemplateAndvalue_ReturnsCorrectText()
        {
            var template = "Hello {0}";
            var name = "Bob";
            var expectedText = $"Hello {name}";

            var builder = new StringBuilder();

            // Act
            var actual = builder
                .Fill(template, name)
                .ToString();

            // Assert
            Assert.AreEqual(expectedText, actual);
        }


        //-------------------------------------------------------------------------
        //
        // AppendIf Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AppendIf_WithTrueAndLine_AddsLine()
        {
            // Arrange
            var existingText = "existing";
            var predicate = true;
            var textToAdd = "new";
            var expected = $"{existingText}{textToAdd}";

            var builder = new StringBuilder();
            builder.Append(existingText);

            // Act
            builder.AppendIf(predicate, textToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AppendIf_WithFalseAndLine_DoesNotAddLine()
        {
            // Arrange
            var existingText = "existing";
            var predicate = false;
            var textToAdd = "new";
            var expected = $"{existingText}";

            var builder = new StringBuilder();
            builder.Append(existingText);

            // Act
            builder.AppendIf(predicate, textToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void AppendIf_WithNoBuilder_ThrowsException()
        {
            var missingBuilderMessage = "The string builder is null.";
            var predicate = false;
            var textToAdd = "new";

            StringBuilder builder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendIf(predicate, textToAdd));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }

        //-------------------------------------------------------------------------
        //
        // AppendIfNot Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AppendIfNot_WithFalseAndLine_AddsLine()
        {
            // Arrange
            var existingText = "existing";
            var predicate = false;
            var textToAdd = "new";
            var expected = $"{existingText}{textToAdd}";

            var builder = new StringBuilder();
            builder.Append(existingText);

            // Act
            builder.AppendIfNot(predicate, textToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AppendIfNot_WithTrueAndLine_DoesNotAddLine()
        {
            // Arrange
            var existingText = "existing line";
            var predicate = true;
            var textToAdd = "new line";
            var expected = $"{existingText}";

            var builder = new StringBuilder();
            builder.Append(existingText);

            // Act
            builder.AppendIfNot(predicate, textToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void AppendIfNot_WithNoBuilder_ThrowsException()
        {
            var missingBuilderMessage = "The string builder is null.";
            var predicate = false;
            var textToAdd = "new";

            StringBuilder builder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendIfNot(predicate, textToAdd));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }

        //-------------------------------------------------------------------------
        //
        // AppendLineIf Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AppendLineIf_WithTrueAndLine_AddsLine()
        {
            // Arrange
            var existingLine = "existing line";
            var predicate = true;
            var lineToAdd = "new line";
            var expected = $"{existingLine}\r\n{lineToAdd}\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.AppendLineIf(predicate, lineToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AppendLineIf_WithFalseAndLine_DoesNotAddLine()
        {
            // Arrange
            var existingLine = "existing line";
            var predicate = false;
            var lineToAdd = "new line";
            var expected = $"{existingLine}\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.AppendLineIf(predicate, lineToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void AppendLineIf_WithNoBuilder_ThrowsException()
        {
            var missingBuilderMessage = "The string builder is null.";
            var predicate = false;
            var lineToAdd = "new line";

            StringBuilder builder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendLineIf(predicate, lineToAdd));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }

        //-------------------------------------------------------------------------
        //
        // AppendLineIfNot Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AppendLineIfNot_WithFalseAndLine_AddsLine()
        {
            // Arrange
            var existingLine = "existing line";
            var predicate = false;
            var lineToAdd = "new line";
            var expected = $"{existingLine}\r\n{lineToAdd}\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.AppendLineIfNot(predicate, lineToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AppendLineIfNot_WithTrueAndLine_DoesNotAddLine()
        {
            // Arrange
            var existingLine = "existing line";
            var predicate = true;
            var lineToAdd = "new line";
            var expected = $"{existingLine}\r\n";

            var builder = new StringBuilder();
            builder.AppendLine(existingLine);

            // Act
            builder.AppendLineIfNot(predicate, lineToAdd);
            var actual = builder.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void AppendLineIfNot_WithNoBuilder_ThrowsException()
        {
            var missingBuilderMessage = "The string builder is null.";
            var predicate = false;
            var lineToAdd = "new line";

            StringBuilder builder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendLineIfNot(predicate, lineToAdd));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }


        //-------------------------------------------------------------------------
        //
        // AppendLineForEach Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AppendLineForEach_WithNoItems_BuildsEmptyString()
        {
            // Arrange
            var expected = string.Empty;
            var noItems = new List<string>();
            var builder = new StringBuilder();

            // Act
            var actual = builder.AppendLineForEach(noItems);

            // Assert
            Assert.IsEmpty(actual.ToString());
        }


        [Test]
        public void AppendLineForEach_WithMultipleItems_BuildsCorrectString()
        {
            // Arrange
            var expected = "one\r\ntwo\r\nthree\r\n";
            var items = new List<string> { "one", "two", "three" };
            var builder = new StringBuilder();

            // Act
            builder.AppendLineForEach(items);

            // Assert
            var actual = builder.ToString();
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AppendLineForEach_WithNullList_ThrowsException()
        {
            // Arrange
            var missingItemsMessage = "The list of items to append cannot be null.";
            List<string> nullItems = null;
            var builder = new StringBuilder();

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendLineForEach(nullItems));

            // Assert
            StringAssert.Contains(expected: missingItemsMessage, actual: ex.Message);
        }

        [Test]
        public void AppendLineForEach_WithNoBuilder_ThrowsException()
        {
            var missingBuilderMessage = "The StringBuilder is null.";
            var noItems = new List<string>();

            StringBuilder builder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendLineForEach(noItems));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }


        //-------------------------------------------------------------------------
        //
        // AppendLineForEachSection Tests (using lambda)
        //
        //-------------------------------------------------------------------------


        [Test]
        public void AppendLineForEach_WithListAndSection_BuildsCorrectOutput()
        {
            // Arrange
            var expectedContent = @"
--------------------------------------------------
Name of Thing: one
Number of Things: 1
--------------------------------------------------
--------------------------------------------------
Name of Thing: two
Number of Things: 2
--------------------------------------------------
--------------------------------------------------
Name of Thing: three
Number of Things: 3
--------------------------------------------------
".TrimStart();
            var list = new List<Tuple<string, int>>
            {
                Tuple.Create<string,int>("one", 1),
                Tuple.Create<string,int>("two", 2),
                Tuple.Create<string,int>("three", 3),
            };
            var divider = new string('-', 50);
            Action<StringBuilder, Tuple<string, int>> sectionBuilder = (b, data) =>
            {
                b
                .AppendLine(divider)
                .AppendLine($"Name of Thing: {data.Item1}")
                .AppendLine($"Number of Things: {data.Item2}")
                .AppendLine(divider);
            };

            var builder = new StringBuilder();

            // Act
            builder.AppendLineForEachSection<Tuple<string, int>>(list, sectionBuilder);
            var actualContent = builder.ToString();

            // Assert
            Assert.AreEqual(expectedContent, actualContent);
        }


        [Test]
        public void AppendLineForEachSection_WithNullBuilder_ThrowsException()
        {
            // Arrange
            var missingBuilderMessage = "The StringBuilder is null.";
            var list = new List<Tuple<string, int>>();
            Action<StringBuilder, Tuple<string, int>> sectionBuilder = (b, data) => b.AppendLine($"{data.Item1}|{data.Item2}");

            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.AppendLineForEachSection(list, sectionBuilder));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }

        [Test]
        public void AppendLineForEachSection_WithNullSectionBuilder_ThrowsException()
        {
            // Arrange
            var missingBuilderMessage = "The delegate that builds a section cannot be null.";
            var list = new List<Tuple<string, int>>();
            Action<StringBuilder, Tuple<string, int>> nullSectionBuilder = null;

            var builder = new StringBuilder();

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => builder.AppendLineForEachSection(list, nullSectionBuilder));

            // Assert
            StringAssert.Contains(expected: missingBuilderMessage, actual: ex.Message);
        }


        //-------------------------------------------------------------------------
        //
        // ConditionalAppend Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void ConditionalAppend_WithNoBuilder_ThrowsException()
        {
            // Arrange
            var noBuilderMessage = "The StringBuilder is null.";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.ConditionalAppend(true, "is true", "is false"));

            // Assert
            StringAssert.Contains(expected: noBuilderMessage, actual: ex.Message);
        }

        [TestCase(true, "true", "false")]
        [TestCase(true, "good", "bad")]
        [TestCase(false, "true", "false")]
        [TestCase(false, "good", "bad")]
        public void ConditionalAppend_WithCustomText_ReturnsCorrectText(bool condition, string trueText, string falseText)
        {
            // Arrange
            var expected = condition ? trueText : falseText;
            var builder = new StringBuilder();

            // Act
            var actual = builder.ConditionalAppend(condition, trueText, falseText).ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(true, "YesNo")]
        [TestCase(true, "TrueFalse")]
        [TestCase(false, "PassFail")]
        [TestCase(false, "OKDash")]
        public void ConditionalAppend_WithStandardText_ReturnsCorrectText(bool condition, string key)
        {
            // Arrange
            var conditionalText = ConditionalText.Predefined
                .Where(i => i.Key == key)
                .First()
                .Value;
            var expected = condition ? conditionalText.TrueText : conditionalText.FalseText;
            var builder = new StringBuilder();

            // Act
            var actual = builder.ConditionalAppend(condition, conditionalText).ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        //-------------------------------------------------------------------------
        //
        // AppendLineIfNotEmpty Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AppendLineIfNotEmpty_WithNoBuilder_ThrowException()
        {
            // Arrange
            var noBuilderMessage = "The StringBuilder is null.";
            StringBuilder nullBuilder = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullBuilder.AppendLineIfNotEmpty("any"));

            // Assert
            StringAssert.Contains(expected: noBuilderMessage, actual: ex.Message);
        }

        [Test]
        public void AppendLineIfNotEmpty_WithNull_DoesNotAppend()
        {
            // Arrange
            var initialValue = "start";
            string appendedValue = null;
            var expected = initialValue;
            var builder = new StringBuilder();
            builder.Append(initialValue);


            // Act
            builder.AppendLineIfNotEmpty(appendedValue);

            // Assert
            Assert.AreEqual(expected, builder.ToString());
        }

        [Test]
        public void AppendLineIfNotEmpty_WithEmptyString_DoesNotAppend()
        {
            // Arrange
            var initialValue = "start";
            var appendedValue = string.Empty;
            var expected = initialValue;
            var builder = new StringBuilder();
            builder.Append(initialValue);


            // Act
            builder.AppendLineIfNotEmpty(appendedValue);

            // Assert
            Assert.AreEqual(expected, builder.ToString());
        }

        [Test]
        public void AppendLineIfNotEmpty_WithWhiteSpace_AppendsWhiteSpace()
        {
            // Arrange
            var initialValue = "start";
            var appendedValue = "    ";
            var expected = initialValue + appendedValue + "\r\n";
            var builder = new StringBuilder();
            builder.Append(initialValue);


            // Act
            builder.AppendLineIfNotEmpty(appendedValue);

            // Assert
            Assert.AreEqual(expected, builder.ToString());
        }

        [Test]
        public void AppendLineIfNotEmpty_WithText_AppendsText()
        {
            // Arrange
            var initialValue = "start";
            var appendedValue = " end";
            var expected = initialValue + appendedValue + "\r\n";
            var builder = new StringBuilder();
            builder.Append(initialValue);


            // Act
            builder.AppendLineIfNotEmpty(appendedValue);

            // Assert
            Assert.AreEqual(expected, builder.ToString());
        }


    }
}
