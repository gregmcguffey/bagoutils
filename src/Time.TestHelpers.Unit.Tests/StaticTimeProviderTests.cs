﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.Time.TestHelpers.Unit.Tests
{
    [TestFixture]
    public class StaticTimeProviderTests
    {

        [Test]
        public void ctor_WithNow_ReturnsNow()
        {
            // Arrange
            var testNow = new DateTime(1969, 7, 21, 2, 56, 34, 444);
            var testUtcNow = testNow.ToUniversalTime();

            var provider = new StaticTimeProvider(testNow);

            // Act
            var actualNow = provider.Now;
            var actualToday = provider.Today;
            var actualUtcNow = provider.UtcNow;

            // Assert
            Assert.AreEqual(testNow, actualNow);
            Assert.AreEqual(testNow.Date, actualToday);
            Assert.AreEqual(testUtcNow, actualUtcNow);
        }

        [Test]
        public void UseUtcNow_WithNow_ReturnsNow()
        {
            // Arrange
            var testUtcNow = new DateTime(1969, 7, 21, 2, 56, 34, 444);
            var testNow = testUtcNow.ToLocalTime();

            var provider = StaticTimeProvider
                .Create()
                .UseUtcNow(testNow);

            // Act
            var actualNow = provider.Now;
            var actualToday = provider.Today;
            var actualUtcNow = provider.UtcNow;

            // Assert
            Assert.AreEqual(testNow, actualNow);
            Assert.AreEqual(testNow.Date, actualToday);
            Assert.AreEqual(testUtcNow, actualUtcNow);
        }

        [Test]
        public void UseYear_WithValue_SetsValue()
        {
            // Arrange
            var testValue = 1969;

            var provider = StaticTimeProvider
                .Create()
                .UseYear(testValue);

            // Act
            var actualValue = provider.Now.Year;

            // Assert
            Assert.AreEqual(testValue, actualValue);
        }

        [Test]
        public void UseMonth_WithValue_SetsValue()
        {
            // Arrange
            var testValue = 7;

            var provider = StaticTimeProvider
                .Create()
                .UseMonth(testValue);

            // Act
            var actualValue = provider.Now.Month;

            // Assert
            Assert.AreEqual(testValue, actualValue);
        }

        [Test]
        public void UseDay_WithValue_SetsValue()
        {
            // Arrange
            var testValue = 20;

            var provider = StaticTimeProvider
                .Create()
                .UseDay(testValue);

            // Act
            var actualValue = provider.Now.Day;

            // Assert
            Assert.AreEqual(testValue, actualValue);
        }

        [Test]
        public void UseHour_WithValue_SetsValue()
        {
            // Arrange
            var testValue = 2;

            var provider = StaticTimeProvider
                .Create()
                .UseHour(testValue);

            // Act
            var actualValue = provider.Now.Hour;

            // Assert
            Assert.AreEqual(testValue, actualValue);
        }

        [Test]
        public void UseMinute_WithValue_SetsValue()
        {
            // Arrange
            var testValue = 56;

            var provider = StaticTimeProvider
                .Create()
                .UseMinute(testValue);

            // Act
            var actualValue = provider.Now.Minute;

            // Assert
            Assert.AreEqual(testValue, actualValue);
        }

        [Test]
        public void UseSecond_WithValue_SetsValue()
        {
            // Arrange
            var testValue = 34;

            var provider = StaticTimeProvider
                .Create()
                .UseSecond(testValue);

            // Act
            var actualValue = provider.Now.Second;

            // Assert
            Assert.AreEqual(testValue, actualValue);
        }

        [Test]
        public void UseMillisecond_WithValue_SetsValue()
        {
            // Arrange
            var testValue = 333;

            var provider = StaticTimeProvider
                .Create()
                .UseMillisecond(testValue);

            // Act
            var actualValue = provider.Now.Millisecond;

            // Assert
            Assert.AreEqual(testValue, actualValue);
        }


        [Test]
        public void Today_WithNowNotSet_ThrowsException()
        {
            // Arrange
            var nowNotSetMessage = "";

            // No time set
            var provider = StaticTimeProvider
                .Create();

            // Act
            DateTime t;
            var ex = Assert.Throws<InvalidOperationException>(() => t = provider.Today);

            // Assert
            StringAssert.Contains(expected: nowNotSetMessage, actual: ex.Message);
        }

    }
}
