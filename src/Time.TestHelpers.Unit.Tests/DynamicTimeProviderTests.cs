﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BagOUtils.Time;
using BagOUtils.Time.TestHelpers;

namespace BagOUtils.Time.TestHelpers.Unit.Tests
{
    [TestFixture]
    public class DynamicTimeProviderTests
    {
        private DateTime baseNow = new DateTime(1969, 7, 20);

        [Test]
        public void ctor_WithBaseTime_ReturnsProvider()
        {
            // Arrange
            var baseNow = this.baseNow;

            // Act
            var provider = new DynamicTimeProvider(baseNow);

            // Assert
            Assert.IsInstanceOf<ITimeProvider>(provider);
        }


        [Test]
        public void Now_WhenCalled_ReturnsRelativeToBaseNow()
        {
            // Arrange
            var baseNow = this.baseNow;
            var provider = new DynamicTimeProvider(baseNow);

            // Act
            var actualNow = provider.Now;

            // Assert
            var diff = actualNow - baseNow;
            Assert.That(diff.TotalSeconds, Is.EqualTo(0).Within(1));
        }

        [Test]
        public void Now_AfterFastforward_ReturnsRelativeToFastforwardNow()
        {
            // Arrange
            var baseNow = this.baseNow;
            var provider = new DynamicTimeProvider(baseNow);

            var forwardNow = baseNow
                .AddYears(3)
                .AddMonths(5)
                .AddDays(21)
                .AddHours(6)
                .AddSeconds(34);

            provider.FastForward(forwardNow);

            // Act
            var actualNow = provider.Now;

            // Assert
            var diff = actualNow - forwardNow;
            Assert.That(diff.TotalSeconds, Is.EqualTo(0).Within(1));
        }


        [Test]
        public void Today_WhenCalled_ReturnsBaseDate()
        {
            // Arrange
            var baseNow = this.baseNow;
            var expectedToday = baseNow.Date;

            var provider = new DynamicTimeProvider(baseNow);

            // Act
            var actualToday = provider.Today;

            // Assert
            Assert.That(expectedToday, Is.EqualTo(actualToday));
        }


        [Test]
        public void UtcNow_WhenCalled_ReturnsUtcBaseNow()
        {
            // Arrange
            var baseNow = this.baseNow;
            var utcNow = baseNow.ToUniversalTime();

            var provider = new DynamicTimeProvider(baseNow);

            // Act
            var actualUtcNow = provider.UtcNow;

            // Assert
            var diff = actualUtcNow - utcNow;
            Assert.That(diff.TotalSeconds, Is.EqualTo(0).Within(1));
        }
    }
}
