﻿using FakeItEasy;
using NUnit.Framework;
using System;
using BagOUtils.Time;

namespace BagOUtils.Unit.Tests
{
    [TestFixture]
    public class TimeProviderTests
    {
        [Test]
        public void StaticConstructor_UsesDefaultProvider()
        {
            var now = TimeProvider.UtcNow;

            Assert.IsInstanceOf<DateTime>(now);
        }

        [Test]
        public void SetCurrent_WithNull_ThrowsException()
        {
            var expectedMessage = "An implementation of the ITimeProvider is required.";

            var ex = Assert.Throws<ArgumentNullException>(() => TimeProvider.SetProvider(null));

            StringAssert.Contains(expectedMessage, ex.Message);
        }

        [Test]
        public void ResetToDefault_UsesCurrentDateTime()
        {
            //-- Setup Fake time provider that returns a date
            //   far in the past.
            var testTimeProvider = A.Fake<ITimeProvider>();
            A.CallTo(() => testTimeProvider.UtcNow).Returns(new DateTime(1969, 7, 20));
            TimeProvider.SetProvider(testTimeProvider);

            var oldNow = TimeProvider.UtcNow;

            TimeProvider.ResetToDefault();

            //-- Get time in present.
            var expectedNow = DateTime.UtcNow;
            var actualNow = TimeProvider.UtcNow;

            Assert.That(oldNow.Ticks, Is.Not.EqualTo(actualNow.Ticks).Within(1000000000));
            Assert.That(expectedNow.Ticks, Is.EqualTo(actualNow.Ticks).Within(1000000000));
        }


        [Test]
        public void Now_WithTestTime_ReturnsTestTime()
        {
            // Arrange
            var testNow = new DateTime(1969, 7, 20, 7, 56, 34);
            var testTimeProvider = A.Fake<ITimeProvider>();
            A.CallTo(() => testTimeProvider.Now).Returns(testNow);
            TimeProvider.SetProvider(testTimeProvider);

            // Act
            var actualNow = TimeProvider.Now;

            // Assert
            Assert.AreEqual(testNow, actualNow);
        }


        [Test]
        public void Today_WithTestDate_ReturnsTestDate()
        {
            // Arrange
            var testNow = new DateTime(1969, 7, 21);
            var testTimeProvider = A.Fake<ITimeProvider>();
            A.CallTo(() => testTimeProvider.Today).Returns(testNow);
            TimeProvider.SetProvider(testTimeProvider);

            // Act
            var actualDate = TimeProvider.Today;

            // Assert
            Assert.AreEqual(testNow, actualDate);
        }


        [Test]
        public void UtcNow_WithTestTime_ReturnsTestTime()
        {
            // Arrange
            var testNow = new DateTime(1969, 7, 21, 2, 56, 34);
            var testTimeProvider = A.Fake<ITimeProvider>();
            A.CallTo(() => testTimeProvider.UtcNow).Returns(testNow);
            TimeProvider.SetProvider(testTimeProvider);

            // Act
            var actualNow = TimeProvider.UtcNow;

            // Assert
            Assert.AreEqual(testNow, actualNow);
        }
    }
}