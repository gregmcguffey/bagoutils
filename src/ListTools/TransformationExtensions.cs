﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.ListTools
{
    /// <summary>
    /// Extension methods that preform transformations on
    /// lists/enumarables/dictionaries in some way.
    /// </summary>
    public static class TransformationExtensions
    {
        /// <summary>
        /// Checks a dictionary variable to see if it is null. If it is,
        /// it returns an empty instance. This does not change the passed in
        /// dictionary. It returns a new dictionary.
        /// </summary>
        public static Dictionary<K, V> NullToEmpty<K, V>(this Dictionary<K, V> dictionaryToCheck)
        {
            var returnDictionary = dictionaryToCheck;
            if (dictionaryToCheck == null)
            {
                returnDictionary = new Dictionary<K, V>();
            }
            return returnDictionary;
        }

        /// <summary>
        /// Give a list, check if it is null. If it is
        /// return an empty list. If not, just return the
        /// original list.
        /// </summary>
        public static List<T> NullToEmpty<T>(this List<T> list)
        {
            var emptyList = new List<T>();
            var normalizedList = (list == null) ? emptyList : list;
            return normalizedList;
        }

        /// <summary>
        /// Get the value of a key or a default value if the key 
        /// hasn't been defined or the value is the default for 
        /// the type.
        /// </summary>
        /// <param name="key">Key to lookup.</param>
        /// <param name="defaultValue">
        /// Value to return if key isn't found or value is default for type.
        /// </param>
        /// <returns></returns>
        public static TValue ItemOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            var value = defaultValue;

            //-- If the dictionary is null, return the default value.
            if (dictionary == null)
            {
                return defaultValue;
            }

            dictionary.TryGetValue(key, out value);
            if (Equals(value, default(TValue)))
            {
                value = defaultValue;
            }

            return value;
        }
        
        /// <summary>
        /// Return the original list or a default list if the original
        /// is null.
        /// </summary>
        /// <param name="defaultList">List to return if no list is provided.</param>
        /// <returns></returns>
        public static IEnumerable<T> OrDefault<T>(this IEnumerable<T> list, IEnumerable<T> defaultList)
        {
            var resolvedList = list;
            if (list == null)
            {
                resolvedList = defaultList;
            }
            return resolvedList;
        }

        /// <summary>
        /// Merge another dictionary with the original. If there are keys
        /// in the dictionary to be merged that match keys in the original,
        /// the values from the dictionary being merged are used.
        /// </summary>
        public static Dictionary<K, V> Merge<K, V>(this Dictionary<K, V> original, Dictionary<K, V> toBeMerged)
        {
            // If either dictionary is null, just create an empty one.
            var originalForcedToEmpty = original.NullToEmpty();
            var toBeMergedForcedToEmpty = toBeMerged.NullToEmpty();

            var newMap = new Dictionary<K, V>(originalForcedToEmpty, originalForcedToEmpty.Comparer);
            foreach (var item in toBeMergedForcedToEmpty)
            {
                // If the right side matches the left, override with right value.
                if (originalForcedToEmpty.ContainsKey(item.Key))
                {
                    newMap[item.Key] = item.Value;
                }
                else
                {
                    newMap.Add(item.Key, item.Value);
                }
            }

            return newMap;
        }

        /// <summary>
        /// Create a union of two dictionaries. This requires that the union
        /// of keys in both dictionaries is unique. I.e. there are no keys
        /// that are in both dictionaries.
        /// </summary>
        public static Dictionary<K, V> Combine<K, V>(this Dictionary<K, V> left, Dictionary<K, V> right)
        {
            // If either dictionary is null, just create an empty one.
            var leftForcedToEmpty = left.NullToEmpty();
            var rightForcedToEmpty = right.NullToEmpty();

            var newMap = new Dictionary<K, V>(leftForcedToEmpty, leftForcedToEmpty.Comparer);
            foreach (var item in rightForcedToEmpty)
            {
                // Keys cannot be in both dictionaries.
                if (leftForcedToEmpty.ContainsKey(item.Key))
                {
                    var exMessage = "The key, '{0}', was found in both dictionaries. Keys must be unique across both dictionaries.".Fill(item.Key);
                    throw new InvalidOperationException(exMessage);
                }
                else
                {
                    newMap.Add(item.Key, item.Value);
                }
            }

            return newMap;
        }

        /// <summary>
        /// Join the items in the provided list into a string,
        /// with each item separated by the indicated delimiter.
        /// </summary>
        /// <param name="list">list of items</param>
        /// <param name="separator">
        /// character(s) that separate each item in the returned string
        /// </param>
        /// <returns>
        /// The items in the list as a string.
        /// </returns>
        public static string JoinIntoString<T>(this IEnumerable<T> list, string separator)
        {
            var normalizedList = (list ?? new List<T> { }).Select(i => i.ToString());
            return String.Join(separator, normalizedList);
        }
    }
}
