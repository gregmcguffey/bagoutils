﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.ListTools
{
    /// <summary>
    /// Extension methods that act on a list or IEnumerable based
    /// on a condition.
    /// </summary>
    public static class ConditionalExtensions
    {
        /// <summary>
        /// Add an item to a list depending on a condition. If
        /// the condition is true then add the true item otherwise
        /// add the false item.
        /// </summary>
        /// <param name="condition">
        /// Condition that determines what item is added.
        /// </param>
        /// <param name="trueItem">
        /// Item to add if condition is true.
        /// </param>
        /// <param name="falseItem">
        /// Item to add if the condition is false.
        /// </param>
        public static List<T> ConditionalAdd<T>(this List<T> list, bool condition, T trueItem, T falseItem)
        {
            list.GuardIsNotNull(nameof(list), () => "The list is null.");

            if (condition)
            {
                list.Add(trueItem);
            }
            else
            {
                list.Add(falseItem);
            }
            return list;
        }

        /// <summary>
        /// Adds an item to a list if the condition is true.
        /// </summary>
        /// <param name="list">List to add item to.</param>
        /// <param name="condition">Predicate for the addition</param>
        /// <param name="item">Item to add.</param>
        /// <returns>Reference to list for fluent use.</returns>
        public static List<T> AddIf<T>(this List<T> list, bool condition, T item)
        {
            list.GuardIsNotNull(nameof(list), () => "The list is null.");

            if (condition)
            {
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// Adds an item to a list if the condition is false.
        /// </summary>
        /// <param name="list">List to add item to.</param>
        /// <param name="condition">Anti-Predicate for the addition</param>
        /// <param name="item">Item to add.</param>
        /// <returns>Reference to list for fluent use.</returns>
        public static List<T> AddIfNot<T>(this List<T> list, bool condition, T item)
        {
            list.GuardIsNotNull(nameof(list), () => "The list is null.");

            list.AddIf(!condition, item);
            return list;
        }

    }
}
