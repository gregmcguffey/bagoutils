﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.ListTools
{
    /// <summary>
    /// Internally used extension methods.
    /// </summary>
    internal static class Extensions
    {
        /// <summary>
        /// Guard that the item is not null.
        /// </summary>
        /// <param name="item">Item guard is called on.</param>
        /// <param name="argumentName">
        /// Name of argument/parameter/state that is being guarded.
        /// </param>
        /// <param name="message">
        /// Exception message if the item is null.
        /// </param>
        /// <returns>
        /// The object is returned to allow a fluent interface with
        /// other guards.
        /// </returns>
        public static object GuardIsNotNull(this object item, string argumentName, Func<string> messageBuilder)
        {
            if (item == null)
            {
                throw new ArgumentNullException(argumentName, messageBuilder());
            }

            return item;
        }

        /// <summary>
        /// Returns a string with replacable parameters filled
        /// with provided values.
        /// </summary>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public static string Fill(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

    }
}
