﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.Time
{

    /// <summary>
    /// Abstraction to get the current date and/or time. This allows for
    /// much easier testing as the current time returned can be
    /// controlled independently of the actual time. The current date
    /// and time are accessed via the static methods, but the
    /// implementation is an instance that allows you to control what is returned.
    /// </summary>
    public static class TimeProvider
    {

        private static ITimeProvider current;

        static TimeProvider()
        {
            TimeProvider.current = new DefaultTimeProvider();
        }

        public static void SetProvider(ITimeProvider timeProvider)
        {
            if (timeProvider == null)
            {
                var exMessage = "An implementation of the ITimeProvider is required.";
                throw new ArgumentNullException("timeProvider", exMessage);
            }

            TimeProvider.current = timeProvider;
        }

        public static void ResetToDefault()
        {
            TimeProvider.current = new DefaultTimeProvider();
        }

        public static DateTime Now
        {
            get
            {
                return TimeProvider.current.Now;
            }
        }

        public static DateTime Today
        {
            get
            {
                return TimeProvider.current.Today;
            }
        }

        public static DateTime UtcNow
        {
            get
            {
                return TimeProvider.current.UtcNow;
            }
        }

    }
}