﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.Time
{
    /// <summary>
    /// Default implementation of the TimeProvider. This uses the system
    /// time (DateTime).
    /// </summary>
    public class DefaultTimeProvider : ITimeProvider
    {
        public DateTime Now
        {
            get { return DateTime.Now; }
        }

        public DateTime Today
        {
            get { return DateTime.Today; }
        }

        public DateTime UtcNow
        {
            get { return DateTime.UtcNow; }
        }

    }
}
