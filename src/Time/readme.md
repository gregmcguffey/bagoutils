# Bag O'Utils Time

This library provides one service: an abstraction of the current date and time. A static class can be
called to get the current moment moment in local time (`Now`) or in UTC (`UtcNow`), the current date
(`Today`). These mirror static methods on `DateTime` and have the same usage.

The default is to use `DateTime` to provide the values. However, this is actually implemented by a class
and can be setup to return any value desired.

The primary purpose of this is to facilitate testing.