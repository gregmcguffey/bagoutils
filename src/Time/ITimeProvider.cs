﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.Time
{
    /// <summary>
    /// Defines properties of a class that returns the
    /// current time/date.
    /// </summary>
    public interface ITimeProvider
    {
        DateTime Now { get; }

        DateTime Today { get; }

        DateTime UtcNow { get; }
    }
}
