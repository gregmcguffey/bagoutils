﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.ListTools.Unit.Tests
{
    [TestFixture]
    public class ConditionalExtensionsTests
    {
        //-------------------------------------------------------------------------
        //
        // ConditionalAdd Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void ConditionalAdd_WithNoList_ThrowsException()
        {
            // Arrange
            var noListMessage = "The list is null.";
            List<int> nullList = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullList.ConditionalAdd(true, 1, 2));

            // Assert
            Assert.That(ex.Message, Does.Contain(noListMessage));
        }


        [Test]
        public void ConditionalAdd_WithTrue_AddsTrueItem()
        {
            // Arrange
            var expected = 1;
            var list = new List<int>();

            // Act
            list.ConditionalAdd(true, 1, 2);

            // Assert
            Assert.That(list[0], Is.EqualTo(expected));
        }

        [Test]
        public void ConditionalAdd_WithFalse_AddsFalseItem()
        {
            // Arrange
            var expected = 2;
            var list = new List<int>();

            // Act
            list.ConditionalAdd(false, 1, 2);

            // Assert
            Assert.That(list[0], Is.EqualTo(expected));
        }


        //-------------------------------------------------------------------------
        //
        // AddIf Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AddIf_WithNullList_ThrowsException()
        {
            // Arrange
            var noListMessage = "The list is null.";
            List<int> nullList = null;

            // Act
            var ex = Assert.Throws<ArgumentNullException>(() => nullList.AddIf(true, 1));

            // Assert
            Assert.That(ex.Message, Does.Contain(noListMessage));
        }

        [Test]
        public void AddIf_WithFalseCondition_ReturnUnchangedList()
        {
            // Arrange
            var original = new List<int> { 1, 2, 3 };
            var underTest = original.Select(i => i);
            var condition = false;

            // Act
            underTest.ToList().AddIf(condition, 4);

            // Assert
            Assert.AreNotSame(original, underTest);
            Assert.That(underTest, Is.EquivalentTo(original));
        }


        [Test]
        public void AddIf_WithTrueCondition_ReturnCorrectlyChangedList()
        {
            // Arrange
            var original = new List<int> { 1, 2, 3 };
            var expected = new List<int> { 1, 2, 3, 4 };

            var underTest = original
                .Select(i => i)
                .ToList();
            var condition = true;

            // Act
            underTest.AddIf(condition, 4);

            // Assert
            Assert.That(underTest, Is.Not.SameAs(expected));
            Assert.That(underTest, Is.EquivalentTo(expected));
        }

        //-------------------------------------------------------------------------
        //
        //  AddIfNot Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void AddIfNot_WithTrueCondition_ReturnUnchangedList()
        {
            // Arrange
            var original = new List<int> { 1, 2, 3 };
            var underTest = original.Select(i => i);
            var condition = true;

            // Act
            underTest.ToList().AddIfNot(condition, 4);

            // Assert
            Assert.That(underTest, Is.Not.SameAs(original));
            Assert.That(underTest, Is.EquivalentTo(original));
        }


        [Test]
        public void AddIfNot_WithFalseCondition_ReturnCorrectlyChangedList()
        {
            // Arrange
            var original = new List<int> { 1, 2, 3 };
            var expected = new List<int> { 1, 2, 3, 4 };

            var underTest = original
                .Select(i => i)
                .ToList();
            var condition = false;

            // Act
            underTest.AddIfNot(condition, 4);

            // Assert
            Assert.That(underTest, Is.Not.SameAs(expected));
            Assert.That(underTest, Is.EquivalentTo(expected));
        }

    }
}
