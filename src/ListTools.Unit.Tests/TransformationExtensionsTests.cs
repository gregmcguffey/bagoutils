﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.ListTools.Unit.Tests
{
    [TestFixture]
    public class TransformationExtensionsTests
    {
        //-------------------------------------------------------------------------
        //
        // NullToEmpty (Dictionary) Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void NulltoEmpty_WithNullDictionary_ReturnEmptyDictionary()
        {
            // Arrange
            var expected = new Dictionary<int, int>();
            Dictionary<int, int> nullList = null;

            // Act
            var actual = nullList.NullToEmpty();

            // Assert
            Assert.That(actual, Is.EquivalentTo(expected));
        }


        [Test]
        public void NullToEmpty_WithDictionary_ReturnsDictionary()
        {
            // Arrange
            var originalDictionary = new Dictionary<int, int>();

            // Act
            var actual = originalDictionary.NullToEmpty();

            // Assert
            Assert.That(actual, Is.SameAs(originalDictionary));
        }


        //-------------------------------------------------------------------------
        //
        // NullToEmpty (List) Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void NulltoEmpty_WithNullList_ReturnEmptyList()
        {
            // Arrange
            var expected = new List<int>();
            List<int> nullList = null;

            // Act
            var actual = nullList.NullToEmpty();

            // Assert
            Assert.That(actual, Is.EquivalentTo(expected));
        }


        [Test]
        public void NullToEmpty_WithList_ReturnsList()
        {
            // Arrange
            var originalList = new List<int> { 1, 2, 3 };

            // Act
            var actual = originalList.NullToEmpty();

            // Assert
            Assert.That(actual, Is.SameAs(originalList));
        }

        //-------------------------------------------------------------------------
        //
        // ItemOrDefault Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void ItemOrDefault_KeyNotFound_ReturnsDefault()
        {
            var dictionary = new Dictionary<int, string>
            {
                { 1, "one" },
                { 2, "two" }
            };
            int unknownKey = 3;
            var defaultValue = "Unknown Key";

            var actualValue = dictionary.ItemOrDefault(unknownKey, defaultValue);

            Assert.AreEqual(defaultValue, actualValue);
        }

        [Test]
        public void ItemOrDefault_NullDictionary_ReturnsDefault()
        {
            Dictionary<int, string> dictionary = null;
            var defaultValue = "Unknown Key";

            string actualValue = dictionary.ItemOrDefault(0, defaultValue);

            Assert.AreEqual(defaultValue, actualValue);
        }

        [Test]
        public void ItemOrDefault_ExistingKey_ReturnsValue()
        {
            var dictionary = new Dictionary<int, string>
            {
                { 1, "one" },
                { 2, "two" }
            };
            int knownKey = 2;
            var defaultValue = "Unknown Key";

            var actualValue = dictionary.ItemOrDefault(knownKey, defaultValue);

            Assert.AreEqual(dictionary[knownKey], actualValue);
        }


        //-------------------------------------------------------------------------
        //
        // OrDefault Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void OrDefault_WithEmptyList_ReturnsDefault()
        {
            // Arrange
            var defaultList = new List<int> { 1, 2, 3 };
            List<int> nullList = null;

            // Act
            var actual = nullList.OrDefault(defaultList);

            // Assert
            Assert.That(actual, Is.SameAs(defaultList));
        }

        [Test]
        public void OrDefault_WithList_ReturnsList()
        {
            // Arrange
            var defaultList = new List<int> { 1, 2, 3 };
            var originalList = new List<int> { 7, 2, 4 };

            // Act
            var actual = originalList.OrDefault(defaultList);

            // Assert
            Assert.That(actual, Is.SameAs(originalList));
        }

        //-------------------------------------------------------------------------
        //
        // Merge Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void Merge_WithUniqueKeys_ReturnsUnion()
        {
            // Arrange
            var dict1 = new Dictionary<string, int>
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 3 },
            };
            var dict2 = new Dictionary<string, int>
            {
                { "four", 4 },
                { "five", 5 },
                { "six", 6 },
            };
            var expectedDict = new Dictionary<string, int>
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 3 },
                { "four", 4 },
                { "five", 5 },
                { "six", 6 },
            };

            // Act
            var actualDict = dict1.Merge(dict2);

            // Assert
            CollectionAssert.AreEquivalent(expectedDict, actualDict);
        }

        [Test]
        public void Merge_WithSharedKeys_UpdatesValuesFromMergedDictionary()
        {
            // Arrange
            var dict1 = new Dictionary<string, int>
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 3 },
            };
            var dict2 = new Dictionary<string, int>
            {
                { "three", 333 },
                { "four", 4 },
                { "five", 5 },
                { "six", 6 },
            };
            var expectedDict = new Dictionary<string, int>
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 333 },
                { "four", 4 },
                { "five", 5 },
                { "six", 6 },
            };

            // Act
            var actualDict = dict1.Merge(dict2);

            // Assert
            CollectionAssert.AreEquivalent(expectedDict, actualDict);
        }


        //-------------------------------------------------------------------------
        //
        // Combine Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void Combine_WithSharedKeys_ThrowsException()
        {
            // Arrange
            var duplicateKeyMessage = "The key, 'three', was found in both dictionaries. Keys must be unique across both dictionaries.";
            var dict1 = new Dictionary<string, int>
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 3 },
            };
            var dict2 = new Dictionary<string, int>
            {
                { "three", 333 },
                { "four", 4 },
                { "five", 5 },
                { "six", 6 },
            };

            // Act
            var ex = Assert.Throws<InvalidOperationException>(() => dict1.Combine(dict2));

            // Assert
            StringAssert.Contains(expected: duplicateKeyMessage, actual: ex.Message);
        }

        [Test]
        public void Combine_WithUniqueKeys_ReturnUnionOfDictionaries()
        {
            // Arrange
            var dict1 = new Dictionary<string, int>
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 3 },
            };
            var dict2 = new Dictionary<string, int>
            {
                { "four", 4 },
                { "five", 5 },
                { "six", 6 },
            };
            var expectedDict = new Dictionary<string, int>
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 3 },
                { "four", 4 },
                { "five", 5 },
                { "six", 6 },
            };

            // Act
            var actualDict = dict1.Combine(dict2);

            // Assert
            CollectionAssert.AreEquivalent(expectedDict, actualDict);
        }


        //-------------------------------------------------------------------------
        //
        // JoinIntoString Tests
        //
        //-------------------------------------------------------------------------

        [Test]
        public void JoinIntoString_WithNullString_ReturnsEmptyString()
        {
            // Arrange
            List<string> nullString = null;
            var expected = string.Empty;

            // Act
            var actual = nullString.JoinIntoString(" ");

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void JoinIntoString_WithSingleItem_ReturnsItem()
        {
            // Arrange
            var item = "hello";
            var singleItemList = new List<string> { item };

            // Act
            var actual = singleItemList.JoinIntoString(" ");

            // Assert
            Assert.That(actual, Is.EqualTo(item));
        }

        [Test]
        public void JoinIntoString_WithListOfStrings_ReturnsString()
        {
            // Arrange
            var list = new List<string> { "a", "b", "c" };
            var expected = "a b c";
            // Act
            var actual = list.JoinIntoString(" ");

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void JoinIntoString_WithListOfInts_ReturnsString()
        {
            // Arrange
            var list = new List<int> { 1, 2, 3 };
            var expected = "1, 2, 3";

            // Act
            var actual = list.JoinIntoString(", ");

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
