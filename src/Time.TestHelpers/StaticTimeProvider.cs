﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BagOUtils.Time;

namespace BagOUtils.Time.TestHelpers
{
    /// <summary>
    /// Time provider that returns a single configured date/time.
    /// </summary>
    public class StaticTimeProvider : ITimeProvider
    {
        /// <summary>
        /// Initiate fluent setup of a static time provider.
        /// </summary>
        /// <returns></returns>
        public static StaticTimeProvider Create()
        {
            return new StaticTimeProvider();
        }

        private DateTime now;
        private bool isNowSet;

        // Used with fluent configuration.
        private int year = 1;
        private int month = 1;
        private int day = 1;
        private int hour = 0;
        private int minute = 0;
        private int second = 0;
        private int millisecond = 0;

        /// <summary>
        /// Private constructor used with fluent setup.
        /// </summary>
        private StaticTimeProvider()
        { }

        /// <summary>
        /// Construct an instance for the specified
        /// date and time.
        /// </summary>
        /// <param name="now"></param>
        public StaticTimeProvider(DateTime now)
        {
            this.SetNow(now);
        }


        //-------------------------------------------------------------------------
        //
        // ITimeProvider Methods
        //
        //-------------------------------------------------------------------------

        public DateTime Now
        {
            get
            {
                this.GuardNowSet();
                return this.now;
            }
        }

        public DateTime Today
        {
            get
            {
                this.GuardNowSet();
                return this.now.Date;
            }
        }

        public DateTime UtcNow
        {
            get
            {
                this.GuardNowSet();
                return this.now.ToUniversalTime();
            }
        }


        //-------------------------------------------------------------------------
        //
        // Fluent Configuration Methods
        //
        //-------------------------------------------------------------------------

        /// <summary>
        /// Set a UTC date/time as the test date/time.
        /// </summary>
        /// <param name="utcNow">UTC test time.</param>
        /// <returns></returns>
        public StaticTimeProvider UseUtcNow(DateTime utcNow)
        {
            this.SetUtcNow(utcNow);
            return this;
        }

        public StaticTimeProvider UseYear(int year)
        {
            return this.ConfigureAndSetNow(() => this.year = year);
        }

        public StaticTimeProvider UseMonth(int month)
        {
            return this.ConfigureAndSetNow(() => this.month = month);
        }

        public StaticTimeProvider UseDay(int day)
        {
            return this.ConfigureAndSetNow(() => this.day = day);
        }

        public StaticTimeProvider UseHour(int hour)
        {
            return this.ConfigureAndSetNow(() => this.hour = hour);
        }

        public StaticTimeProvider UseMinute(int minute)
        {
            return this.ConfigureAndSetNow(() => this.minute = minute);
        }

        public StaticTimeProvider UseSecond(int second)
        {
            return this.ConfigureAndSetNow(() => this.second = second);
        }

        public StaticTimeProvider UseMillisecond(int millisecond)
        {
            return this.ConfigureAndSetNow(() => this.millisecond = millisecond);
        }

        //-------------------------------------------------------------------------
        //
        // Private helpers
        //
        //-------------------------------------------------------------------------

        /// <summary>
        /// Configure one of the date/time values and
        /// then build a now based on whatevery is currnetly
        /// configured
        /// </summary>
        /// <param name="doConfig">Lambda tha tsets one of the date/time values.</param>
        /// <returns></returns>
        private StaticTimeProvider ConfigureAndSetNow(Action doConfig)
        {
            doConfig();
            var now = this.BuildCurrentNow();
            this.SetNow(now);
            return this;
        }

        /// <summary>
        /// Set the static moment that is to be used and track
        /// that it has been set.
        /// </summary>
        /// <param name="now"></param>
        private void SetNow(DateTime now)
        {
            this.now = now;
            this.isNowSet = true;
        }

        /// <summary>
        /// Set the UTC static moment that is to be used and track
        /// that it has been set.
        /// </summary>
        /// <param name="utcNow"></param>
        private void SetUtcNow(DateTime utcNow)
        {
            this.now = utcNow.ToLocalTime();
            this.isNowSet = true;
        }

        /// <summary>
        /// Build a date time that reflects the current date/time
        /// that is being fluently configured.
        /// </summary>
        /// <returns></returns>
        private DateTime BuildCurrentNow()
        {
            var now = new DateTime(this.year, this.month, this.day, this.hour, this.minute, this.second, this.millisecond);
            return now;
        }

        /// <summary>
        /// Guard that the user has explicitly set
        /// a date and time somehow.
        /// </summary>
        private void GuardNowSet()
        {
            if (!this.isNowSet)
            {
                var exMessage = "The static date/time has not been set.";
                throw new InvalidOperationException(exMessage);
            }
        }
    }
}
