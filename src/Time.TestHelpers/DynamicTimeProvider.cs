﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BagOUtils.Time;

namespace BagOUtils.Time.TestHelpers
{
    public class DynamicTimeProvider : ITimeProvider
    {

        private DateTime referenceNow;
        private TimeSpan cachedDifference;

        public DynamicTimeProvider(DateTime baseNow)
        {
            this.SetReferenceNow(baseNow);
        }

        //-------------------------------------------------------------------------
        //
        // ITimeProvider Implementation
        //
        //-------------------------------------------------------------------------

        public DateTime Now
        {
            get
            {
                return this.GetTestNow();
            }
        }

        public DateTime Today
        {
            get
            {
                return this.GetTestNow().Date;
            }
        }

        public DateTime UtcNow
        {
            get
            {
                return this.GetTestNow().ToUniversalTime();
            }
        }

        //-------------------------------------------------------------------------
        //
        // Dynamic Time Methods
        //
        //-------------------------------------------------------------------------

        public void FastForward(DateTime newNow)
        {
            this.SetReferenceNow(newNow);
        }

        //-------------------------------------------------------------------------
        //
        // Private Helpers
        //
        //-------------------------------------------------------------------------

        private DateTime GetTestNow()
        {
            var actualNow = DateTime.Now;
            return actualNow - this.cachedDifference;
        }

        private void SetReferenceNow(DateTime newNow)
        {
            this.referenceNow = newNow;
            var currentNow = DateTime.Now;
            this.cachedDifference = currentNow - this.referenceNow;
        }
    }
}
