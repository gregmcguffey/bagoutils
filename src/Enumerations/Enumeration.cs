﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace BagOUtils.Enumerations
{
    /// <summary>
    /// Defines base abstract class for a class based
    /// enumeration. This allows for additional data 
    /// and behavior to be encapsulated within an 
    /// enumeration.
    /// </summary>
    public abstract class Enumeration
    {
        //-------------------------------------------------------------------------
        //
        // Static Methods
        //
        //-------------------------------------------------------------------------

        /// <summary>
        /// Get all the defined enumerations for a specific enumeration.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        /// Enumerable collection of the enumerations defined for the
        /// specified type.
        /// </returns>
        /// <remarks>
        /// Note that this uses reflection to find all the public, static
        /// fields that are subclasses of this enumeration. Originally,
        /// the code newed up an instance to get the fields, but this is
        /// not necessary (at least not as of .net 4.5).
        /// </remarks>
        public static IEnumerable<T> GetAll<T>() where T : Enumeration
        {
            var type = typeof(T);
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            foreach (var info in fields)
            {
                // This gets the value of the static field,
                // which will be one of the enum values. 
                // The GetValue( null ) indicates we should get
                // a value from a static field.
                var locatedEnumValue = info.GetValue(null) as T;

                if (locatedEnumValue != null)
                {
                    yield return locatedEnumValue;
                }
            }
        }

        /// <summary>
        /// Get all the defined enumerations for a specific enumeration.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        /// Enumerable collection of the enumerations defined for the
        /// specified type.
        /// </returns>
        /// <remarks>
        /// Note that this uses reflection to find all the public, static
        /// fields that are subclasses of this enumeration.
        /// </remarks>
        public static IEnumerable GetAll(Type enumerationType)
        {
            var fields = enumerationType.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            foreach (var info in fields)
            {
                // This gets the value of the static field,
                // which will be one of the enum values. 
                // The GetValue( null ) indicates we should get
                // a value from a static field.
                var locatedEnumValue = info.GetValue(null);

                if (locatedEnumValue != null)
                {
                    yield return locatedEnumValue;
                }
            }
        }

        public static TEnumeration FromValue<TEnumeration>(string code)
          where TEnumeration : Enumeration
        {
            var matchingItem = parse<TEnumeration, string>(code, "value", item => (item.Code.CompareTo(code) == 0));
            return matchingItem;
        }

        public static Enumeration Parse(Type enumerationType, string code)
        {
            Func<Enumeration, bool> predicate = item => item.Code == code;
            var matchingItem = GetAll(enumerationType).Cast<Enumeration>().FirstOrDefault(predicate);

            if (matchingItem == null)
            {
                var message = string.Format("'{0}' is not a valid value in {1}", code, enumerationType);
                throw new InvalidOperationException(message);
            }
            return matchingItem;
        }


        //-------------------------------------------------------------------------
        //
        // Instance 
        //
        //-------------------------------------------------------------------------

        protected Enumeration(string code)
        {
            this.Code = code;
        }

        /// <summary>
        /// Code that defines this enumeration. 
        /// </summary>
        /// <remarks>
        /// This must be unique among the enumaration values
        /// and it should match the property name exactly.
        /// </remarks>
        public string Code { get; private set; }

        public override string ToString()
        {
            return this.Code;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Enumeration;

            if (other == null)
            {
                return false;
            }

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = this.Code.Equals(other.Code);

            return typeMatches && valueMatches;
        }

        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }

        public int CompareTo(object other)
        {
            return Code.CompareTo(((Enumeration)other).Code);
        }


        //-------------------------------------------------------------------------
        //
        // Static Protected
        //
        //-------------------------------------------------------------------------
        protected static T parse<T, K>(K value, string description, Func<T, bool> predicate)
          where T : Enumeration
        {
            var matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem == null)
            {
                var message = string.Format("'{0}' is not a valid {1} in {2}", value, description, typeof(T));
                throw new InvalidOperationException(message);
            }

            return matchingItem;
        }
    }
}
