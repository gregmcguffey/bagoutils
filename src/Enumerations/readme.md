# Bag O'Utils Enumeration

This provides a class based enumeration (instead of the value based .NET Enum). This allows for an
enumeration to have multiple values and to have behaviors. 

This can allow for both richer data in an enumeration and also reduces/eliminates the need for `switch`
statements. This reduces code complexity and maintanence as logic related to an enumeration is isolated
in a single class.