﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.Enumerations
{
    /// <summary>
    /// Base class for an enumeration that requires a displayable
    /// name. This sort of enumeration is typcially user facing.
    /// </summary>
    public abstract class DisplayEnumeration : Enumeration
    {
        protected DisplayEnumeration(string code, string displayName)
            : base(code)
        {
            this.DisplayName = displayName;
        }

        /// <summary>
        /// Return the user friendly name for the enumeration.
        /// </summary>
        public virtual string DisplayName { get; private set; }


        /// <summary>
        /// Given a display name, get the matching enumeration.
        /// </summary>
        /// <typeparam name="T">Type of enumeration that contains the display name.</typeparam>
        /// <param name="displayName">Name to find.</param>
        /// <returns>Enumeration item with the indicated display name.</returns>
        public static T FromDisplayName<T>(string displayName) 
            where T : DisplayEnumeration
        {
            var matchingItem = parse<T, string>(displayName, "display name", item => item.DisplayName == displayName);
            return matchingItem;
        }

        /// <summary>
        /// Given a display name, get the matching enumeration.
        /// </summary>
        /// <param name="enumerationType">Type of enumeration that contains the display name.</param>
        /// <param name="displayName">Name to find.</param>
        /// <returns>Enumeration item with the indicated display name.</returns>
        /// <remarks>
        /// This is the non-generic version, when the type is determined at runtime.
        /// </remarks>
        public static Enumeration ParseDisplay(Type enumerationType, string displayName)
        {
            Func<DisplayEnumeration, bool> predicate = item => item.DisplayName == displayName;
            var matchingItem = GetAll(enumerationType).Cast<DisplayEnumeration>().FirstOrDefault(predicate);

            if (matchingItem == null)
            {
                var message = string.Format("'{0}' is not a valid value in {1}", displayName, enumerationType);
                throw new InvalidOperationException(message);
            }
            return matchingItem;
        }

    }
}
