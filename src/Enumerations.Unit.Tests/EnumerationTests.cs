﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.Enumerations.Unit.Tests
{
    [TestFixture]
    public class EnumerationTests
    {
        [Test]
        public void ToString_ReturnsCode()
        {
            var expectedCode = "Yard (3 ft.)";

            var actualDisplay = TestEnumeration.Yard.ToString();

            Assert.AreEqual(expectedCode, actualDisplay);
        }

        [Test]
        public void GetAll_Generic_ReturnsAllEnumerations()
        {
            var expectedEnums = new List<TestEnumeration>
            {
                TestEnumeration.Feet,
                TestEnumeration.Yard,
                TestEnumeration.Mile,
            };

            var actualEnums = Enumeration.GetAll<TestEnumeration>();

            Assert.That(expectedEnums, Is.EquivalentTo(actualEnums.ToList()));
        }

        [Test]
        public void GetAll_NonGeneric_ReturnsAllEnumerations()
        {
            var expectedEnums = new List<TestEnumeration>
            {
                TestEnumeration.Feet,
                TestEnumeration.Yard,
                TestEnumeration.Mile,
            };

            var actualEnums = Enumeration.GetAll(typeof(TestEnumeration));

            Assert.That(expectedEnums, Is.EquivalentTo(actualEnums.OfType<TestEnumeration>()));
        }

        [Test]
        public void Equals_WithSameObject_ReturnsTrue()
        {
            var testEnum = TestEnumeration.Feet;

            var result = TestEnumeration.Feet.Equals(testEnum);

            Assert.IsTrue(result);
        }

        [Test]
        public void Equals_WithDifferentObject_ReturnsFalse()
        {
            var testEnum = TestEnumeration.Yard;

            var result = TestEnumeration.Feet.Equals(testEnum);

            Assert.IsFalse(result);
        }

        [Test]
        public void Equals_WithNull_ReturnsFalse()
        {
            TestEnumeration testEnum = null;

            var result = TestEnumeration.Feet.Equals(testEnum);

            Assert.IsFalse(result);
        }

        [Test]
        public void FromValue_WithValidValue_ReturnEnum()
        {
            var code = "Feet";
            var expectedEnum = TestEnumeration.Feet;

            var actualEnum = Enumeration.FromValue<TestEnumeration>(code);

            Assert.AreEqual(expectedEnum, actualEnum);
        }

        [Test]
        public void FromValue_WithInvalidValue_ThrowsException()
        {
            var code = "Hundred";
            var expectedMessage = string.Format("'Hundred' is not a valid value in {0}"
                                                , typeof(TestEnumeration));

            var ex = Assert.Throws<InvalidOperationException>(() => Enumeration.FromValue<TestEnumeration>(code));

            StringAssert.Contains(expectedMessage, ex.Message);
        }

        [TestCase("Yard", "Feet", 1)]
        [TestCase("Yard", "Yard", 0)]
        [TestCase("Feet", "Mile", -1)]
        public void CompareTo_Item_ReturnsCorrectResult(string baseCode
                                                       , string compareCode
                                                       , int expectedResult)
        {

            // Prepare
            var baseEnum = Enumeration.FromValue<TestEnumeration>(baseCode);
            var comparedEnum = Enumeration.FromValue<TestEnumeration>(compareCode);

            // Act
            var actualResult = baseEnum.CompareTo(comparedEnum);

            // Assert
            // This is doing string comparisons. Useful for sorting.
            Assert.AreEqual(expectedResult, actualResult);
        }

        /// <summary>
        /// This test demonstrates the ability to use an enumeration 
        /// and not need a switch statement.
        /// </summary>
        [Test]
        public void CodeReturnsEnum_WhenGivenCode_AndExecutesCode()
        {
            // Arrange
            // This could be in a config file or a db table.
            var code = "Mile";
            var otherNumber = 5;
            var expectedNumber = 5 * 5280;
            var expectedAsString = "Mile (5280 ft.)";

            // Old way. Any time a value is added or changed,
            // You'll have to find all the switch statements 
            // and fix them.
            var oldEnum = (OldEnum)Enum.Parse(typeof(OldEnum), code);
            var conversion = 0;
            switch (oldEnum)
            {
                case OldEnum.Feet:
                    conversion = 1;
                    break;
                case OldEnum.Yard:
                    conversion = 3;
                    break;
                case OldEnum.Mile:
                    conversion = 5280;
                    break;
            }
            var valueFromOldWay = otherNumber * conversion;

            // And any other functionality you have to add yourself
            // (though extension methods are very nice for this).
            // This opens possibility to implement it differently
            // if not careful.
            var oldAsString = $"{oldEnum.ToString()} ({conversion} ft.)";


            // New Way:
            var foundEnum = Enumeration.FromValue<TestEnumeration>(code);
            var valueFromNewWay = foundEnum.ConvertToFeet(otherNumber);
            var newAsString = foundEnum.ToString();

            // Assert
            Assert.AreEqual(expectedNumber, valueFromOldWay);
            Assert.AreEqual(expectedNumber, valueFromNewWay);
            Assert.AreEqual(expectedAsString, oldAsString);
            Assert.AreEqual(expectedAsString, newAsString);
        }
    }

    //-------------------------------------------------------------------------
    //
    // Test Enumerations
    //
    //-------------------------------------------------------------------------


    internal class TestEnumeration : Enumeration
    {
        public static TestEnumeration Feet = new TestEnumeration("Feet", 1, "ft");
        public static TestEnumeration Yard = new TestEnumeration("Yard", 3, "yd");
        public static TestEnumeration Mile = new TestEnumeration("Mile", 5280, "mi");


        public TestEnumeration(string code, int asFeet, string unit)
          : base(code)
        {
            this.Unit = unit;
            this.AsFeet = asFeet;
        }

        public int AsFeet;
        public string Unit;

        public int ConvertToFeet(int otherValue)
        {
            return this.AsFeet * otherValue;
        }

        public override string ToString()
        {
            return $"{this.Code} ({this.AsFeet} ft.)";
        }
    }

    internal enum OldEnum
    {
        Feet,
        Yard,
        Mile,
    }
}

