﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.Enumerations.Unit.Tests
{
    [TestFixture]
    public class DisplayEnumerationTests
    {
        [Test]
        public void FromDisplayName_WithValidDisplay_ReturnsEnum()
        {
            var displayName = "One";
            var expectedEnum = TestDisplayEnumeration.One;

            var actualEnum = DisplayEnumeration.FromDisplayName<TestDisplayEnumeration>(displayName);

            Assert.AreSame(expectedEnum, actualEnum);
        }

        [Test]
        public void ParseDisplay_WithValidDisplay_ReturnsEnum()
        {
            var displayName = "One";
            var expectedEnum = TestDisplayEnumeration.One;

            var actualEnum = DisplayEnumeration.ParseDisplay(typeof(TestDisplayEnumeration), displayName);

            Assert.AreSame(expectedEnum, actualEnum);
        }
    }


    //-------------------------------------------------------------------------
    //
    // Test Enumeration
    //
    //-------------------------------------------------------------------------

    internal class TestDisplayEnumeration : DisplayEnumeration
    {
        public static TestDisplayEnumeration One = new TestDisplayEnumeration("-1-", "One", 1, "Integer");
        public static TestDisplayEnumeration Two = new TestDisplayEnumeration("-2-", "Two", 2, "Integer");
        public static TestDisplayEnumeration Three = new TestDisplayEnumeration("-3-", "Three", 3, "Integer");


        public TestDisplayEnumeration(string code, string displayName, int value, string unit)
          : base(code, displayName)
        {
            this.Unit = unit;
            this.Value = value;
        }

        public int Value;
        public string Unit;
    }

}
