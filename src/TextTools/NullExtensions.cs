﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Extension methods for strings to handle nulls.
    /// </summary>
    public static class NullExtensions
    {
        /// <summary>
        /// Provide a default if a string is null.
        /// </summary>
        public static string DefaultIfNull(this string text)
        {
            var fixedText = text;
            if (text == null)
            {
                fixedText = string.Empty;
            }
            return fixedText;
        }

        /// <summary>
        /// Returns if a string is null or empty. This
        /// is a fluent wrapper for the string.IsNullOrEmpty
        /// method.
        /// </summary>
        /// <param name="text">string to test</param>
        /// <returns>
        /// True if the string is null or empty.
        /// </returns>
        public static bool IsNullOrEmpty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        /// <summary>
        /// Returns if a string is null, empty or only whitespace.
        /// This is a fluent wrapper for the string.IsNullOrWhiteSpace method.
        /// </summary>
        /// <param name="text">String to test</param>
        /// <returns>True if the string is null, empty or only whitespace.</returns>
        public static bool IsNullOrWhiteSpace(this string text)
        {
            return string.IsNullOrWhiteSpace(text);
        }
    }

}
