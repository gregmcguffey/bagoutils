﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Extensions methods that transform text in some way.
    /// </summary>
    public static class TransformExtensions
    {
        /// <summary>
        /// Returns a string with replacable parameters filled
        /// with provided values.
        /// </summary>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public static string Fill(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        /// <summary>
        /// Collapse all white space to a space.
        /// </summary>
        public static string CollapseWhitespace(this string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return String.Empty;
            }
            else
            {
                return String.Join(" ", s.Split((string[])null, StringSplitOptions.RemoveEmptyEntries));
            }
        }

        /// <summary>
        /// Converts a null string into an empty string. Any other
        /// string is just returned.
        /// </summary>
        /// <param name="text">Text to ensure it is not null.</param>
        /// <returns>
        /// If text is null, an empty string, otherwise the provided
        /// string is returned.
        /// </returns>
        public static string NullToEmpty(this string text)
        {
            var fixedOrOriginalText = text;
            if (text.IsNullOrEmpty())
            {
                fixedOrOriginalText = string.Empty;
            }
            return fixedOrOriginalText;
        }

        /// <summary>
        /// Returns either the text or if the text is null/empty
        /// the default text.
        /// </summary>
        /// <param name="text">Text to process.</param>
        /// <param name="defaultText">Default text.</param>
        public static string OrDefault(this string text, string defaultText)
        {
            var processedText = text;
            if (processedText.IsNullOrEmpty())
            {
                processedText = defaultText;
            }
            return processedText;
        }
    
        /// <summary>
        /// Ensure that a string has as the indicated prefix.
        /// </summary>
        /// <param name="prefix">
        /// String that must be prefixed to the beginning of the text.
        /// </param>
        /// <returns>
        /// Value with dot prepended. Null, empty and whitespace only
        /// values will return just a dot.
        /// </returns>
        public static string EnsurePrefixed(this string value, string prefix)
        {
            var dottedValue = value.NullToEmpty().Trim();
            if (!dottedValue.StartsWith(prefix))
            {
                dottedValue = $"{prefix}{dottedValue}";
            }
            return dottedValue;
        }

        /// <summary>
        /// Ensure that a string has the indicated suffix.
        /// </summary>
        /// <returns>
        /// Value with dot prepended. Null, empty and whitespace only
        /// values will return just a dot.
        /// </returns>
        public static string EnsureSuffixed(this string value, string suffix)
        {
            var dottedValue = value.NullToEmpty().Trim();
            if (!dottedValue.EndsWith(suffix))
            {
                dottedValue = $"{dottedValue}{suffix}";
            }
            return dottedValue;
        }

        /// <summary>
        /// Remove some known text from the end of a string.
        /// </summary>
        /// <param name="textToRemove">
        /// Text that should be removed if it exists at the end of the text.
        /// </param>
        /// <returns>
        /// Value with the extraneous text removed if it ended with
        /// the text . If the value is null, an empty string is returned.
        /// </returns>
        public static string RemoveFromEnd(this string value, string textToRemove)
        {
            var cleanedValue = value.NullToEmpty();
            var normalizedRemoveText = textToRemove.NullToEmpty();
            if (cleanedValue.EndsWith(normalizedRemoveText))
            {
                cleanedValue = cleanedValue.Substring(0, cleanedValue.Length - normalizedRemoveText.Length);
            }
            return cleanedValue;
        }

        /// <summary>
        /// Remove some known text from the start of a string.
        /// </summary>
        /// <param name="textToRemove">Text to remove from start of string.</param>
        /// <returns>
        /// Value with the extraneous text removed if it started with
        /// the text. If the value is null, an empty string is returned.
        /// </returns>
        public static string RemoveFromStart(this string text, string textToRemove)
        {
            // Searches are case sensitive.
            var cleanedText = text.NullToEmpty();
            var cleanedTextToRemove = textToRemove.NullToEmpty();
            var normalizedText = cleanedText.ToLower();
            var checkText = cleanedTextToRemove.ToLower();
            if (normalizedText.StartsWith(checkText))
            {
                cleanedText = cleanedText.Substring(cleanedTextToRemove.Length);
            }
            return cleanedText;
        }

        /// <summary>
        /// Quote the text with double quotes.
        /// </summary>
        public static string Quote(this string text)
        {
            return text.Tokenize("\"");
        }

        /// <summary>
        /// Tokenize the text with with the provided delimiter.
        /// </summary>
        public static string Tokenize(this string text, string commonDelimiter)
        {
            return text.Tokenize(commonDelimiter, commonDelimiter);
        }

        /// <summary>
        /// Tokenize the text with with the provided delimiters.
        /// </summary>
        public static string Tokenize(this string text, string leftDelimiter, string rightDelimiter)
        {
            return $"{leftDelimiter}{text}{rightDelimiter}";
        }

        /// <summary>
        /// Split a string into lines.
        /// </summary>
        /// <returns>Enumeration of lines in the text.</returns>
        public static IEnumerable<string> SplitIntoLines(this string text, bool removeEmptyLines = false)
        {
            if (text.IsNullOrEmpty())
            {
                return new List<string>();
            }

            var options = removeEmptyLines
                ? StringSplitOptions.RemoveEmptyEntries
                : StringSplitOptions.None;

            var delimiters = new[] { "\r\n", "\r", "\n" };

            return text.Split(delimiters, options);
        }

        /// <summary>
        /// Convert line breaks (new line, carriage return or
        /// line feed) into the HTML 'br' tag.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ConvertLineBreakToHTML(this string text)
        {
            const string newlinesRegex = @"(\r\n|\r|\n)";

            string result = System.Text.RegularExpressions.Regex.Replace(text, newlinesRegex, "<br />");

            return result;
        }
    }
}
