﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Extension method that allows for string coalescing using 
    /// a fluent API. This works for null, empty and white space strings.
    /// </summary>
    /// <remarks>
    /// The standard null coalescer in C# (??)
    /// only works with null strings. This works
    /// with an extension method to provide a 
    /// nice fluent interface.
    /// </remarks>
    public static class StringCoalescer
    {
        /// <summary>
        /// Coalesces the current string value with another
        /// string value, returning the first one that is
        /// not null, empty or only white space.
        /// </summary>
        /// <param name="currentValue"></param>
        /// <param name="nextValue"></param>
        /// <returns>
        /// Current value if it contains a value. Otherwise it
        /// returns the next value.
        /// </returns>
        /// <remarks>
        /// Note that this means that if no values are provided in 
        /// the chain that are not null/empty/whitespace, the last
        /// value in chain is returned.
        /// </remarks>
        public static string Or(this string currentValue, string nextValue)
        {
            var isNullOrEmpty = string.IsNullOrWhiteSpace(currentValue);
            var actualValue = isNullOrEmpty ? nextValue : currentValue;

            return actualValue;
        }
    }
}
