﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Defines some common sets of conditional text that can be
    /// displayed to indicate the outcome of a test or validation.
    /// </summary>
    public class ConditionalText
    {
        public static ConditionalText YesNo = ConditionalText.CreateDiscoverable("YesNo", "Yes", "No");
        public static ConditionalText TrueFalse = ConditionalText.CreateDiscoverable("TrueFalse", "True", "False");
        public static ConditionalText PassFail = ConditionalText.CreateDiscoverable("PassFail", "Pass", "Fail");
        public static ConditionalText OKDash = ConditionalText.CreateDiscoverable("OKDash", "OK", "--");

        public static Dictionary<string, ConditionalText> Predefined { get; private set; }

        public static ConditionalText CreateDiscoverable(string key, string trueText, string falseText)
        {
            var conditionalText = new ConditionalText(key, trueText, falseText);

            if(Predefined == null)
            {
                Predefined = new Dictionary<string, ConditionalText>();
            }
            Predefined.Add(key, conditionalText);

            return conditionalText;
        }

        /// <summary>
        /// Create a custom conditonal text item.
        /// </summary>
        /// <param name="trueText">Text to return when a condition is true.</param>
        /// <param name="falseText">Text to return when a condition is false.</param>
        /// <returns></returns>
        public ConditionalText(string trueText, string falseText)
            : this("any", trueText, falseText)
        { }

        /// <summary>
        /// Create a conditional text object that is discoverable via the key.
        /// </summary>
        /// <param name="key">Key used to identify the conditional text.</param>
        /// <param name="trueText">Text to return when a condition is true.</param>
        /// <param name="falseText">Text to return when a condition is false.</param>
        public ConditionalText(string key, string trueText, string falseText)
        {
            this.Key = key;
            this.TrueText = trueText;
            this.FalseText = falseText;
        }

        public string Key { get; set; }

        /// <summary>
        /// Text returned if a condition is true.
        /// </summary>
        public string TrueText { get; private set; }

        /// <summary>
        /// Text returned if a condition is false.
        /// </summary>
        public string FalseText { get; private set; }

        /// <summary>
        /// Resolve the text to use based on the condition.
        /// </summary>
        /// <param name="condition">Condition that determines which text to use.</param>
        /// <returns>TrueText if condition if true, FalseText otherwise.</returns>
        public string ResolveText(bool condition)
        {
            if (condition)
            {
                return this.TrueText;
            }
            else
            {
                return this.FalseText;
            }
        }
    }
}
