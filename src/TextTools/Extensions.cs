﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Internally used extension methods.
    /// </summary>
    internal static class Extensions
    {
        /// <summary>
        /// Guard that the item is not null.
        /// </summary>
        /// <param name="item">Item guard is called on.</param>
        /// <param name="argumentName">
        /// Name of argument/parameter/state that is being guarded.
        /// </param>
        /// <param name="message">
        /// Exception message if the item is null.
        /// </param>
        /// <returns>
        /// The object is returned to allow a fluent interface with
        /// other guards.
        /// </returns>
        public static object GuardIsNotNull(this object item, string argumentName, Func<string> messageBuilder)
        {
            if (item == null)
            {
                throw new ArgumentNullException(argumentName, messageBuilder());
            }

            return item;
        }

        /// <summary>
        /// Guard that a numeric value is equal to or above the minimum value.
        /// </summary>
        /// <param name="value">
        /// Value to test.
        /// </param>
        /// <param name="argumentName">
        /// Name of argument/parameter/state being tested.
        /// </param>
        /// <param name="minimumValue">
        /// Minimum acceptable value.
        /// </param>
        /// <returns>
        /// The value is returned if it is valid.
        /// </returns>
        public static T GuardMinimum<T>(this T value, string argumentName, T minimumValue, Func<string> messageBuilder)
            where T : IComparable<T>
        {
            if (value.LessThan(minimumValue))
            {
                throw new ArgumentOutOfRangeException(argumentName, messageBuilder());
            }

            return value;
        }

        /// <summary>
        /// Determine if the other value is less than 
        /// the base value.
        /// </summary>
        /// <typeparam name="T">Any comparable type</typeparam>
        /// <param name="baseValue">the extended value</param>
        /// <param name="otherValue">the value to test</param>
        /// <returns>
        /// True if the base value is less than the other value.
        /// </returns>
        public static bool LessThan<T>(this T baseValue, T otherValue)
          where T : IComparable<T>
        {
            return baseValue.CompareTo(otherValue) < 0;
        }

    }
}
