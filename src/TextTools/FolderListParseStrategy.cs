﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Define strategy used to parse a list of folders into a final
    /// list that matches the a strategy. The strategy revolves around
    /// whether or not the drive and/or file are present/desired.
    /// </summary>
    public class FolderListParseStrategy
    {
        public static FolderListParseStrategy UseAll = new FolderListParseStrategy(false, false);
        public static FolderListParseStrategy IgnoreDrive = new FolderListParseStrategy(true, false);
        public static FolderListParseStrategy IgnoreFile = new FolderListParseStrategy(false, true);
        public static FolderListParseStrategy IgnoreDriveAndFile = new FolderListParseStrategy(true, true);

        private FolderListParseStrategy(bool ignoreDrive, bool ignoreFile)
        {
            this.SkipDrive = ignoreDrive;
            this.SkipFile = ignoreFile;
        }

        public bool SkipDrive { get; private set; }
        public bool SkipFile { get; private set; }

        /// <summary>
        /// Give a list of folders, parse the list and return
        /// the folders according to strategy.
        /// </summary>
        /// <param name="fullPathList"></param>
        /// <returns></returns>
        public IEnumerable<string> Parse(List<string> fullPathList)
        {
            var fileFolderList = fullPathList
                .Skip(this.SkipDrive ? 1 : 0)
                .Reverse()
                .Skip(this.SkipFile ? 1 : 0)
                .Reverse();
            return fileFolderList;
        }
    }
}
