﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Provides string formatting with use of named arguments. The
    /// data for the arguments is provided by an object with
    /// properties matching the name.
    /// </summary>
    public static class NamedFormatter
    {
        public static string NamedFormat(this string format, object source)
        {
            format.GuardIsNotNull("format", () => "The format string cannot be null.");

            StringBuilder result = new StringBuilder(format.Length * 2);

            using (var reader = new StringReader(format))
            {
                StringBuilder expression = new StringBuilder();
                int @char = -1;

                State state = State.OutsideExpression;
                do
                {
                    @char = reader.Read();

                    // Parse the characters, taking action
                    // based on the current state. Set a
                    // current state based on the parsing
                    // also append data to the result.
                    switch (state)
                    {
                        case State.OutsideExpression:
                            switch (@char)
                            {
                                case -1:
                                    state = State.End;
                                    break;

                                case '{':
                                    // Start of expression or first of double if literal.
                                    state = State.OnOpenBracket;
                                    break;

                                case '}':
                                    // First of double for literal.
                                    state = State.OnCloseBracket;
                                    break;

                                default:
                                    result.Append((char)@char);
                                    break;
                            }
                            break;

                        case State.OnOpenBracket:
                            switch (@char)
                            {
                                case -1:
                                    // Missing closing bracket, no name
                                    throw new FormatException("No name was specified for a formatting argument and the closing bracket is missing.");

                                case '{':
                                    // Double swirling brackets if literal swirly bracket is needed.
                                    result.Append('{');
                                    state = State.OutsideExpression;
                                    break;

                                case '}':
                                    // We have an empty name, not allowed.
                                    throw new FormatException("No name was specified for a formatting argument.");

                                default:
                                    expression.Append((char)@char);
                                    state = State.InsideExpression;
                                    break;
                            }
                            break;

                        case State.InsideExpression:
                            switch (@char)
                            {
                                case -1:
                                    // Missing closing bracket
                                    throw new FormatException("Closing bracket is missing.");

                                case '}':
                                    result.Append(OutExpression(source, expression.ToString()));
                                    expression.Length = 0;
                                    state = State.OutsideExpression;
                                    break;

                                default:
                                    expression.Append((char)@char);
                                    break;
                            }
                            break;

                        case State.OnCloseBracket:
                            switch (@char)
                            {
                                case '}':
                                    // This is second of double, so literal is added.
                                    result.Append('}');
                                    state = State.OutsideExpression;
                                    break;

                                default:
                                    // This state is only every hit if the
                                    // last character was a closing bracket
                                    // that was not part of an expression,
                                    // thus the only valid character is
                                    // another closing bracket.
                                    throw new FormatException("Closing bracket with no matching opening bracket.");
                            }
                            break;

                        default:
                            throw new InvalidOperationException("Invalid state.");
                    }
                } while (state != State.End);
            }

            return result.ToString();
        }

        /// <summary>
        /// Return the value that will replace the expression. The
        /// value is derived from the source object.
        /// </summary>
        /// <param name="source">
        /// Data source object.
        /// </param>
        /// <param name="expression">
        /// Expression indicating the property of the object that
        /// contains the data.
        /// </param>
        /// <returns>
        /// The data that is represented by the expression.
        /// </returns>
        private static string OutExpression(object source, string expression)
        {
            string format = "";

            int colonIndex = expression.IndexOf(':');
            if (colonIndex > 0)
            {
                format = expression.Substring(colonIndex + 1);
                expression = expression.Substring(0, colonIndex);
            }

            // Setup format string. An ZLS when passed
            // to DataBinder.Eval is equivalent to not
            // passing it.
            string preparedFormat = "";
            if (!format.IsNullOrEmpty())
            {
                preparedFormat = "{0:" + format + "}";
            }

            try
            {
                return (DataBinder.Eval(source, expression, preparedFormat)).ToString() ?? "";
            }
            catch (HttpException)
            {
                throw new FormatException();
            }
        }

        private enum State
        {
            OutsideExpression,
            OnOpenBracket,
            InsideExpression,
            OnCloseBracket,
            End
        }
    }
}
