﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Extension methods that allow easy manipulation of
    /// strings as paths or file names.
    /// </summary>
    public static class PathRelatedExtensions
    {
        /// <summary>
        /// Returns a string that represents the base path
        /// having the additional path appended to it.
        /// This is a fluent wrapper for the Path.Combine
        /// method.
        /// </summary>
        /// <param name="basePath">
        /// String representing the base path.
        /// </param>
        /// <param name="additionalPath">
        /// Additional path to append to the base path.
        /// </param>
        /// <returns>
        /// Combination of the base path and additional path.
        /// </returns>
        public static string AddPath(this string basePath, string additionalPath)
        {
            basePath.GuardIsNotNull("basePath", () => "The base path cannot be null.");
            additionalPath.GuardIsNotNull("additionalPath", () => "The additional path cannot be null.");

            var combinedPath = Path.Combine(basePath, additionalPath);

            return combinedPath;
        }

        /// <summary>
        /// Return the full path to an ancestor folder, based on a
        /// reference folder. This returns the path up until the
        /// reference folder is found. The reference folder itself can
        /// be included as well.
        /// </summary>
        /// <param name="fullPath">
        /// Full path that includes some ancestor path.
        /// </param>
        /// <param name="referenceFolder">
        /// Folder that determines how much of the path to return.
        /// </param>
        /// <param name="includeReferenceFolder">
        /// Indicates if the reference folder is included. If false,
        /// the last folder will be the parent of the reference folder.
        /// </param>
        /// <returns>
        /// Full path up to (and optionally including) the reference folder
        /// </returns>
        /// <example>
        /// Shows how the default returns parent of reference folder. 
        /// var fullPath = @"C:\Test\Project\bin\debug"; 
        /// var solutionFolder = fullPath.PathToAncestor( "Project");
        /// solutionFolder.Dump(); // ""C:\Test" 
        /// </example>
        /// <example>
        /// Shows how using the includeReferenceFolder argument includes the reference folder also.
        /// var fullPath = @"C:\Test\Project\bin\debug"; 
        /// var projectFolder = fullPath.PathToAncestor( "Project", includeReferenceFolder: true ); 
        /// projectFolder.Dump(); // "C:\Test\Project"
        /// </example>"
        public static string PathToAncestor(this string fullPath, string referenceFolder, bool includeReferenceFolder = false, bool noFileInPath = false)
        {
            fullPath.GuardIsNotNull("fullPath", () => "The base path that calls this method cannot be null.");
            referenceFolder.GuardIsNotNull("referenceFolder", () => "The reference folder cannot be null.");

            // If the path doesn't include a file, we need to 
            // let the folder list know to return the last folder (which normally is the file).
            var folderParseStrategy = noFileInPath
                ? FolderListParseStrategy.UseAll
                : FolderListParseStrategy.IgnoreFile;
            var folderList = fullPath.FolderList(folderParseStrategy);
            var ancestorFolderList = new List<string>();
            var continueAddingToPath = true;

            // Process the list of folders, adding folders 
            // until we get to the reference folder.
            folderList.ForEach(folder =>
            {
                if (continueAddingToPath)
                {
                    var isReferencePath = folder == referenceFolder;
                    if (isReferencePath)
                    {
                        if (includeReferenceFolder)
                        {
                            ancestorFolderList.Add(folder);
                        }

                        continueAddingToPath = false;
                    }
                    else
                    {
                        ancestorFolderList.Add(folder);
                    }
                }
            });

            var ancestorPath = string.Join(Path.DirectorySeparatorChar.ToString(), ancestorFolderList);

            return ancestorPath;
        }

        /// <summary>
        /// Returns a list of folders that comprise the 
        /// path. This operates on a string and does not
        /// validate that this is a valid path.
        /// </summary>
        /// <param name="path">
        /// String representing the path to parse.
        /// </param>
        /// <returns>
        /// List of folders, without the drive or filename.
        /// </returns>
        /// <remarks>
        /// Relative paths aren't handled differently. I.e.
        /// if you pass in "..\..\projects\dapper\tools" and
        /// then use 'projects' as reference folder, the list 
        /// will be two entries of '..'.
        /// </remarks>
        public static List<string> FolderList(this string path, FolderListParseStrategy strategy = null)
        {
            path.GuardIsNotNull("path", () => "No path was provided.");

            strategy = strategy ?? FolderListParseStrategy.IgnoreDriveAndFile;

            var driveFoldersFile = path.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                                       .ToList();
            var folders = strategy
                .Parse(driveFoldersFile)
                .ToList();

            return folders;
        }

        /// <summary>
        /// Return the CodeBase version of a path. This will
        /// convert a normal windows path into the format
        /// returned by the Assembly.CodeBase property.
        /// </summary>
        /// <param name="path">
        /// Path to convert.
        /// </param>
        /// <returns>
        /// CodeBase formatted path.
        /// </returns>
        public static string ToCodeBase(this string path)
        {
            path.GuardIsNotNull("path", () => "No path was supplied.");

            var codeBase = "File:///{0}".Fill(path.Replace(@"\", "/"));

            return codeBase;
        }
    }
}
