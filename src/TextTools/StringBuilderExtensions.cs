﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Extensions methods used with StringBuilder.
    /// </summary>
    public static class StringBuilderExtensions
    {
        /// <summary>
        /// Append one or more spaces to a string builder.
        /// </summary>
        /// <param name="size">Number of spaces to add. Default is 1.</param>
        /// <returns></returns>
        public static StringBuilder Space(this StringBuilder builder, int size = 1)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The string builder is null.");
            size.GuardMinimum(nameof(size), 0, () => $"The minimum number of spaces is zero. {size} is less than zero.");

            var space = new string(' ', size);
            builder.Append(space);
            return builder;
        }

        /// <summary>
        /// Append a blank line to the string builder.
        /// </summary>
        /// <remarks>
        /// Alias for AppendLine().
        /// </remarks>
        public static StringBuilder BlankLine(this StringBuilder builder)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The string builder is null.");
            builder.AppendLine();
            return builder;
        }

        /// <summary>
        /// Append a divider, which is a line of repeating text, to the builder.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="dividerSize"></param>
        /// <param name="divider"></param>
        /// <returns></returns>
        public static StringBuilder AppendDivider(this StringBuilder builder, int dividerSize, char divider = '-')
        {
            builder.GuardIsNotNull(nameof(builder), () => "The string builder is null.");
            dividerSize.GuardMinimum(nameof(dividerSize), 0, () => $"The minimum size of a divider is zero. {dividerSize} is less than zero.");

            if (dividerSize > 0)
            {
                var dividerText = new string(divider, dividerSize);
                builder.AppendLine(dividerText);
            }
            return builder;
        }

        /// <summary>
        /// Add an indent to text being built.
        /// </summary>
        /// <param name="builder">StringBuilder to add indent to.</param>
        /// <param name="size">Size of indent.</param>
        /// <remarks>
        /// This simply appends the indicated number of spaces. It
        /// is really only an indent if this occurs at the beginning 
        /// of a line.
        /// </remarks>
        public static StringBuilder Indent(this StringBuilder builder, int size)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");

            var indent = new string(' ', size);
            builder.Append(indent);
            return builder;
        }

        /// <summary>
        /// Add an indent to text being built if the condition is true.
        /// </summary>
        /// <param name="builder">StringBuilder to add indent to.</param>
        /// <param name="condition">
        /// Condition that must be true to add indent.
        /// </param>
        /// <param name="size">Size of indent.</param>
        /// <remarks>
        /// This simply appends the indicated number of spaces. It is
        /// really only an indent if this occurs at the beginning of a line.
        /// </remarks>
        public static StringBuilder IndentIf(this StringBuilder builder, bool condition, int size)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");

            if (condition)
            {
                builder.Indent(size);
            }
            return builder;
        }

        /// <summary>
        /// Add an indent to text being built if the condition is false.
        /// </summary>
        /// <param name="builder">StringBuilder to add indent to.</param>
        /// <param name="condition">
        /// Condition that must be false to add indent.
        /// </param>
        /// <param name="size">Size of indent.</param>
        /// <remarks>
        /// This simply appends the indicated number of spaces. It is
        /// really only an indent if this occurs at the beginning of a line.
        /// </remarks>
        public static StringBuilder IndentIfNot(this StringBuilder builder, bool condition, int size)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");

            if (!condition)
            {
                builder.Indent(size);
            }
            return builder;
        }

        /// <summary>
        /// Fill a template with data and append to the string builder.
        /// </summary>
        /// <param name="template">String.Format template.</param>
        /// <param name="args">Data to fill into template.</param>
        /// <returns></returns>
        public static StringBuilder Fill(this StringBuilder builder, string template, params object[] args)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");

            var filledText = template.Fill(args);
            builder.Append(filledText);
            return builder;
        }

        /// <summary>
        /// Append text to the string builder if the predicate is true.
        /// </summary>
        /// <param name="predicate">Expression that if true, indicates the text is added.</param>
        /// <param name="textToAdd">Text to add.</param>
        public static StringBuilder AppendIf(this StringBuilder builder, bool predicate, string textToAdd)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The string builder is null.");
            if (predicate)
            {
                builder.Append(textToAdd);
            }
            return builder;
        }

        /// <summary>
        /// Append text to the string builder if the predicate is false.
        /// </summary>
        /// <param name="predicate">Expression that if false, indicates the text is added.</param>
        /// <param name="textToAdd">Text to add.</param>
        public static StringBuilder AppendIfNot(this StringBuilder builder, bool predicate, string textToAdd)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The string builder is null.");
            if (!predicate)
            {
                builder.Append(textToAdd);
            }
            return builder;
        }

        /// <summary>
        /// Append a line to the string builder if the predicate is true.
        /// </summary>
        /// <param name="predicate">Expression that if true, indicates the line is added.</param>
        /// <param name="lineToAdd">Line of text to add.</param>
        public static StringBuilder AppendLineIf(this StringBuilder builder, bool predicate, string lineToAdd)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The string builder is null.");
            if (predicate)
            {
                builder.AppendLine(lineToAdd);
            }
            return builder;
        }

        /// <summary>
        /// Append a line to the string builder if the predicate is false.
        /// </summary>
        /// <param name="predicate">Expression that if false, indicates the line is added.</param>
        /// <param name="lineToAdd">Line of text to add.</param>
        public static StringBuilder AppendLineIfNot(this StringBuilder builder, bool predicate, string lineToAdd)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The string builder is null.");
            if (!predicate)
            {
                builder.AppendLine(lineToAdd);
            }
            return builder;
        }

        /// <summary>
        /// Appends the text if the text is not empty.
        /// </summary>
        /// <param name="text">Text to append.</param>
        public static StringBuilder AppendLineIfNotEmpty(this StringBuilder builder, string text)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");

            if (!string.IsNullOrEmpty(text))
            {
                builder.AppendLine(text);
            }

            return builder;
        }

        /// <summary>
        /// Append a line to the string builder for each item in the 
        /// collection of items.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="items">Any IEnumerable of strings.</param>
        /// <returns></returns>
        public static StringBuilder AppendLineForEach(this StringBuilder builder, IEnumerable<string> items)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");
            items.GuardIsNotNull(nameof(items), () => "The list of items to append cannot be null.");

            items
                .ToList()
                .ForEach(item => builder.AppendLine(item));
            return builder;
        }

        /// <summary>
        /// Append a section of lines to the string builder for each
        /// item in the collection of items.
        /// </summary>
        /// <typeparam name="T">Type of data in the list.</typeparam>
        /// <param name="builder">
        /// StringBuilder that will have lines appended (a section) for
        /// each item in the list.
        /// </param>
        /// <param name="items">
        /// List of items that contains data used to build the sections.
        /// </param>
        /// <param name="sectionBuilder">
        /// Delegate that appends to the string builder based on the
        /// data in the list.
        /// </param>
        /// <returns>Fluently returns the StringBuilder.</returns>
        public static StringBuilder AppendLineForEachSection<T>(this StringBuilder builder, IEnumerable<T> items, Action<StringBuilder, T> sectionBuilder)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");
            items.GuardIsNotNull(nameof(items), () => "The list of items to append cannot be null.");
            sectionBuilder.GuardIsNotNull(nameof(sectionBuilder), () => "The delegate that builds a section cannot be null.");

            items
                .ToList()
                .ForEach(item => sectionBuilder(builder, item));
            return builder;
        }

        /// <summary>
        /// Append text to a string builder based on a condition. If
        /// the condition is true then append the trueText otherwise
        /// append the false text.
        /// </summary>
        /// <param name="condition">
        /// Condition that determines what text is appended.
        /// </param>
        /// <param name="conditionalText">
        /// Defines true text and false text.
        /// </param>
        public static StringBuilder ConditionalAppend(this StringBuilder builder, bool condition, ConditionalText conditionalText)
        {
            builder.GuardIsNotNull(nameof(builder), () => "The StringBuilder is null.");

            builder.Append(conditionalText.ResolveText(condition));

            return builder;
        }

        /// <summary>
        /// Append text to a string builder based on a condition. If
        /// the condition is true then append the trueText otherwise
        /// append the false text.
        /// </summary>
        /// <param name="condition">
        /// Condition that determines what text is appended.
        /// </param>
        /// <param name="trueText">
        /// Text to append if condition is true.
        /// </param>
        /// <param name="falseText">
        /// Text to append if the condition is false.
        /// </param>
        public static StringBuilder ConditionalAppend(this StringBuilder builder, bool condition, string trueText, string falseText)
        {
            var conditionalText = new ConditionalText(trueText, falseText);
            builder.ConditionalAppend(condition, conditionalText);

            return builder;
        }

    }
}
