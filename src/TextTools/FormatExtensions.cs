﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagOUtils.TextTools
{
    /// <summary>
    /// Extension methods that format text.
    /// </summary>
    public static class FormatExtensions
    {
        /// <summary>
        /// Indent the provided text by the indicated number of spaces.
        /// </summary>
        /// <param name="size">Number of spaces to indent.</param>
        /// <returns></returns>
        public static string Indent(this string text, int size = 4)
        {
            size.GuardMinimum("size", 0, () => $"The minimum indent is zero but {size} was provided.");

            // It is valid that the text is null or empty. If either
            // is the case, then return an empty string.
            var processedText = string.Empty;
            if (!text.IsNullOrEmpty())
            {
                var indent = new string(' ', size);
                processedText = $"{indent}{text}";
            }

            return processedText;
        }

        /// <summary>
        /// Indent every line in a set of lines
        /// </summary>
        public static IEnumerable<string> Indent(this IEnumerable<string> lines, int size = 4)
        {
            lines.GuardIsNotNull(nameof(lines), () => "The lines variable is null.");
            return lines
                .Select(l => l.Indent(size));
        }
    }
}
